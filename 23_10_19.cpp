using Functor = std::function<void()>;
class TCPSever
{
private:
    uint64_t _next_id;                                  // 一个自增长的id
    EventLoop _baseloop;                                // 主线程专门用来监听的基础对象
    Acceptor _acceptor;                                 // 一个用来进行监听连接的对象
    LoopThreadPool _loops;                              // 管理多个线程的线程池
    std::unordered_map<uint64_t, PtrConnection> _conns; // 管理多个Connection对象的
    int _timeout;                                       // 连接的同一时间，超过该时间未响应的就是非活跃连接
    bool _enable_inactive_release;                      // 设置是否启动非活跃连接销毁
    int _port;
    // 给Connection对象设置的回调函数，由外部使用者设置
    using ConnectCallBack = std::function<void(const PtrConnection&)>;
    using CloseCallBack = std::function<void(const PtrConnection&)>;
    using MessageCallBack = std::function<void(const PtrConnection&, Buffer*)>;
    using AnyEventCallBack = std::function<void(const PtrConnection&)>;
    ConnectCallBack _connect;
    CloseCallBack _close;
    MessageCallBack _msg;
    AnyEventCallBack _any_event;

private:
    // 监听收到了一个新链接就需要调用该函数
    void NewConnection(int fd)
    {
        _next_id++;
        PtrConnection con(new Connection(_loops.GetEventLoop(), _next_id, fd));
        con->SetMessageCallBack(_msg);
        con->SetCloseCallBack(_close);
        con->SetConnectCallBack(_connect);
        con->SetAnyEventCallBack(_any_event);
        con->SetSrvCloseCallBack(std::bind(&TCPSever::RemoveFromCons, this, con));
        // 设置读监控和回调函数
        if (_enable_inactive_release == true)
        {
            con->EnableInactiveRelease(10);
        }
        con->Established();
        _cons.insert(std::make_pair(con_id, con));
    }
    // 移除需要通过该函数将Connection从_cons中移除
    void RemoveFromConsInLoop(const PtrConnection& con)
    {
        uint64_t id = con->Id();
        auto it = _cons.find(id);
        if (it != _cons.end())
        {
            _cons.erase(it);
        }
    }
    void RemoveFromCons(const PtrConnection& con)
    {
        baseloop.RunInLoop(std::bind(&TCPSever::RemoveFromConsInLoop, this, con));
    }
    void RunAfterInLoop(const Functor& task, int timeout)
    {
        _next_id++;
        _baseloop.TimerAdd(_next_id, timeout, task);
    }

public:
    TCPSever(int port) : _port(port),
        _next_id(0),
        _enable_inactive_release(false),
        _acceptor(&_baseloop, _port),
        _loops(&baseloop)
    {
    }
    void SetThreadCount(int count)
    {
        _loops.SetThreadCount(count);
    } // 设置线程个数
    void SetConnectCallBack(const ConnectCallBack& cb) { _connect = cb; }
    void SetCloseCallBack(const CloseCallBack& cb) { _close = cb; }
    void SetMessageCallBack(const MessageCallBack& cb) { _msg = cb; }
    void SetAnyEventCallBack(const AnyEventCallBack& cb) { _any_event = cb; }
    // 启动非活跃连接销毁
    void EnableInactiveRelease(int timeout)
    {
        _timeout = timeout;
        _enable_inactive_release = true;
    }
    // 添加定时任务
    void RunAfter(const Functor& task, int timeout)
    {
        _baseloop.RunInLoop(std::bind(&TCPSever::RunAfterInLoop, this, task, timeout));
    }
    // 启动Sever
    void Start()
    {
        _loops.Create();
        _acceptor.Listen();
        _baseloop.Start();
    }
};
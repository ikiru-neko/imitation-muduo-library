//#include "../Sever.hpp"
//
//int main()
//{
//    Socket client;
//    // 连接
//    bool ret = client.CreateClient(8080, "127.0.0.1");
//    assert(ret == true);
//    char buf[1024] = { "Hello Wrold!" };
//    while (1)
//    {
//        char recv[1024] = { 0 };
//        client.Send(buf, sizeof(buf));
//        client.Recv(recv, 1023);
//        std::cout << recv << std::endl;
//        sleep(1);
//    }
//    return 0;
//}

////#include "../Sever.hpp"
//void HandClose(Channel* channel)
//{
//    std::cout << "close" << std::endl;
//    int fd = channel->Fd();
//    channel->Remove();
//    close(fd);
//    delete channel;
//}
//void HandRead(Channel* channel)
//{
//    int fd = channel->Fd();
//    char buffer[1024] = { 0 };
//    int ret = recv(fd, buffer, 1023, 0);
//    if (ret <= 0)
//    {
//        HandClose(channel);
//        return;
//    }
//    std::cout << buffer << std::endl;
//    channel->EnableWrite();
//}
//void HandWrite(Channel* channel)
//{
//    int fd = channel->Fd();
//    const char* buffer = "I am Sever";
//    int ret = send(fd, buffer, 1023, 0);
//    if (ret <= 0)
//    {
//        HandClose(channel);
//        return;
//    }
//    channel->EnableRead();
//}
//void HandError(Channel* channel)
//{
//    ERROR_LOG("HandError start");
//    HandClose(channel);
//}
//void HandEvent(Channel* channel)
//{
//    std::cout << "一个事件发生了!" << std::endl;
//}
//
//// listen_channel的可读回调函数
//// 参数需要一个监听套接字，用来查看连接
//// 还需要一个Epoller*用来进行管理
//void Acceptor(Epoller* poller, Channel* channel)
//{
//    std::cout << "Accept!" << std::endl;
//    int fd = channel->Fd();
//    int newfd = accept(fd, NULL, NULL);
//    if (newfd < 0)
//    {
//        ERROR_LOG("ACCEPTOR FAILED!");
//        return;
//    }
//    Channel* new_channel = new Channel(poller, newfd);
//    new_channel->SetReadcb(std::bind(HandRead, new_channel));
//    new_channel->SetWritecb(std::bind(HandWrite, new_channel));
//    new_channel->SetErrorcb(std::bind(HandError, new_channel));
//    new_channel->SetClosecb(std::bind(HandClose, new_channel));
//    new_channel->SetEventcb(std::bind(HandEvent, new_channel));
//    new_channel->EnableRead();
//}
//
//int main()
//{
//    // Socket Sever;
//    // bool ret = Sever.CreateSever(8080);
//    // assert(ret == true);
//    // int cli_fd = Sever.Accept();
//    // Socket cli(cli_fd);
//    // while (1)
//    // {
//    //     char buf[1024] = {0};
//    //     cli.Recv(buf, 1023);
//    //     std::cout << buf << std::endl;
//    // }
//    Socket Sever;
//    Sever.CreateSever(8080);
//    Epoller poller;
//    Channel server_channel(&poller, Sever.Fd());
//    server_channel.SetReadcb(std::bind(Acceptor, &poller, &server_channel));
//    server_channel.EnableRead();
//    while (1)
//    {
//        std::vector<Channel*> actives;
//        poller.Poll(&actives);
//        std::cout << "就绪！" << std::endl;
//        for (auto& t : actives)
//        {
//            //就绪了就运行对应的事件
//            t->HandlerEvent();
//        }
//    }
//    return 0;
//}
class HttpSever
{
private:
    using Handler = std::function<void(const HttpRequest&, HttpResponse*)>;
    using Handlers = std::vector<std::pair<std::regex, Handler>>;
    Handlers _get_route;
    Handlers _post_route;
    Handlers _put_route;
    Handlers _delete_route;
    TCPSever _server;
    std::string _basedir;
private:
    void WriteResponse(const PtrConnection con, HttpRequest& request, HttpResponse& response)
    {
        // 1.完善头部字段
        //  1.长短连接
        //  2.是否有正文以及正文长度
        // 3.是否有正文以及正文类型
        if (request.Close() == true)
        {
            // true表示是长连接
            if (response.HasHeader("Connection") == false)
            {
                response.SetHeader("Connection", "keep-alive");
            }
        }
        else
        {
            if (response.HasHeader("Connection") == false)
            {
                response.SetHeader("Connection", "close");
            }
        }
        if (response._body.empty() == false && response.HasHeader("Content-Length") == false)
        {
            response.SetHeader("Content-Length", std::to_string(request._body.size()));
        }
        if (response._body.empty() == false && response.HasHeader("Content-Type") == false)
        {
            response.SetHeader("Content-Type", "otect/stream");
        }
        if (response._redirect_flag == true)
        {
            response.SetHeader("Location", response._redirect_url);
        }
        // 2.按照Http协议格式填充字符串
        std::stringstream rep_str;
        rep_str << request._version << " " << response._statu << Util::GetDesc(response._statu) << "\r\n";
        for (auto head : response._hearders)
        {
            rep_str << head.first << ":" << head.second << "\r\n";
        }
        rep_str << "\r\n";
        rep_str << response._body;
        // 3.发送数据
        con->Send(rep_str.str().c_str(), rep_str.str().size());
    }
    void ErrorHandler(HttpRequest& request, HttpResponse& response)
    {
        std::string body;
        body += "<html>";
        body += "<head>";
        body += "<meta http-equiv='Content-Type' content='text/html;charset=utf-8'>";
        body += "</head>";
        body += "<body>";
        body += "<h1>";
        body += std::to_string(response._statu);
        body += " : ";
        body += Util::GetDesc(response._statu);
        body += "</h1>";
        body += "</body>";
        body += "</html>";
        return;
    }
    bool IsFileHandler(HttpRequest& request)
    {
        //1.首先判断是否有静态的根目录
        if (_basedir.empty() == true)
        {
            return false;
        }
        //2.只有GET/HEAD方法才是静态的资源请求
        if (request._method != "GET" || request._method != "HEAD")
        {
            return false;
        }
        //3.判断是否是有效的资源路径
        // 有可能请求的path只是一个单纯的 / ，这样就表示访问index.html
        // 也有可能就是单纯的访问 /image/a.jpg之类的，需要在前面加上默认根目录 --./www.root/image/a.jpg
        std::string req_path = _basedir + request._path;
        if (request._path.back() == '/')
        {
            req_path += "index.html";
        }
        if (Util::IsValidPath(req_path) == false)
        {
            return false;
        }
        //4.判断该文件是否存在
        if (Util::IsRegular(req_path) == false)
        {
            return false;
        }
        request._path = req_path;
        return true;
    }
    // 静态请求
    void FileHandler(HttpRequest& request, HttpResponse* response)
    {
        //通过request的资源路径找到对应的资源，然后填充到response的body中去
        //并且还要填充一个Content-Type的头部字段
        bool ret = Util::ReadFile(request._path, &(response->_body));
        if (ret == false)
        {
            DEBUG_LOG("ReadFile FAILED!!");
            return;
        }
        std::string mine = Util::ExcMine(request._path);
        response->SetHeader("Content-Type", mine);
        return;
    }
    // 功能性请求，根据不同的请求来使用不同的路由表
    void Dispatcher(HttpRequest& request, HttpResponse* response, Handlers& handlers)
    {
        // 通过正则库来对request的资源请求进行匹配
        for (auto& handler : handlers)
        {
            const std::regex& re = handler.first;
            Handler functor = handler.second;
            bool ret = std::regex_match(request._path, request._matches, re);
            //若是匹配失败，就直接continue来循环下一次
            if (ret == false)
                continue;
            //若是成功了，就直接调用functor进行业务处理
            return functor(request, response);
        }
        //最后若是循环结束依旧没有找到，就直接设置404
        response->_statu = 404;
    }
    // 开始路由，查看是静态请求还是功能性请求
    void Route(HttpRequest& request, HttpResponse* response)
    {
        // GET HEAD都是静态请求，先默认进行FileHandler
        if (IsFileHandler(request) == true)
        {
            return FileHandler(request, response);
        }
        if (request._method == "GET" || request._method == "HEAD")
        {
            return Dispatcher(request, response, _get_route);
        }
        else if (request._method == "PUT")
        {
            return Dispatcher(request, response, _put_route);
        }
        else if (request._method == "POST")
        {
            return Dispatcher(request, response, _post_route);
        }
        else if (request._method == "DELETE")
        {
            return Dispatcher(request, response, _delete_route);
        }
        // 若是都没有，就修该状态码
        response->_statu = 405; // Method Not Allowed;
    }
    // 设置上下文
    void OnConnected(PtrConnection con)
    {
        con->SetContext(HttpContext());
        DEBUG_LOG("NEW CONNECTION:%p", con.get());
    }
    // 解析并处理请求
    void OnMessage(PtrConnection con, Buffer* buf)
    {
        while (buf->ReadAbleSize() > 0)
        {
            // 1.获取连接的上下文
            HttpContext* context = con->GetContext()->get<HttpContext>();
            // 2.处理上下文请求，并获得HttpRequest对象
            //   1.有可能请求出错了
            //   2.请求没出错
            context->RecvHttpRequest(buf);
            HttpRequest& request = context->GetRequest();
            HttpResponse response(context->GetResponseStatu());
            if (context->GetResponseStatu() >= 400)
            {
                // 大于等于400说明出现了错误，就关闭连接并返回
                ErrorHandler(request, response);
                WriteResponse(con, request, response);
                context->Reset();
                return;
            }
            if (context->GetRecvStatu() != Recv_Http_All);
            {
                return;
            }
            // 3.进行路由，并进行业务处理
            Route(request, &response);
            // 4.进行业务处理后，获得HttpResponse对象，并发送过去
            WriteResponse(con, request, response);
            context->Reset();
            // 5.若是短连接，就需要直接shutdown这个连接
            if (request.Close() == false)
                con->Shutdown();
        }
    }

public:
    HttpSever(int port, int timeout = 30)
        :_server(port)
    {
        _server.EnableInactiveRelease(timeout);
        _server.SetConnectCallBack(std::bind(&HttpSever::OnConnected, this, std::placeholders::_1));
        _server.SetMessageCallBack(std::bind(&HttpSever::OnMessage, this, std::placeholders::_1, std::placeholders::_2));
    }
    // 设置对应请求的路由表
    // 其中路由表所记录的函数名并不是单纯的一个函数名，而是一个正则表达式
    void Get(std::string& pattern, Handler handler)
    {
        _get_route.push_back(std::make_pair(std::regex(pattern), handler));
    }
    void Post(std::string& pattern, Handler handler) {
        _post_route.push_back(std::make_pair(std::regex(pattern), handler));
    }
    void Put(std::string& pattern, Handler handler) {
        _put_route.push_back(std::make_pair(std::regex(pattern), handler));
    }
    void Delete(std::string& pattern, Handler handler) {
        _delete_route.push_back(std::make_pair(std::regex(pattern), handler));
    }
    // 设置线程个数
    void SetThreadCount(int count)
    {
        _server.SetThreadCount(count);
    }
    // 启动服务器
    void Listen()
    {
        _server.Start();
    }
};
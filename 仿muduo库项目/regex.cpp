// #include <iostream>
// #include <string>
// #include <regex>


// int main()
// {
//     std::string s = "/wwwroot/123456";
//     std::regex e("/wwwroot/(\\d+)"); // 判断是否是以/wwwroot/开头，并且后面跟着多个数字
//     std::smatch matches;

//     bool flag = std::regex_match(s, matches, e);
//     if (flag == false)
//     {
//         std::cout << "match failed " << std::endl;
//         return -1;
//     }
//     for (auto &t : matches)
//     {
//         std::cout << t << std::endl;
//     }
//     return 0;
// }

#include<iostream>
#include<string>
#include<regex>

int main()
{
    std::string s = "GET /wwwroot/a/b/c?usrname=张三&pwd=124533 HTTP/1.1\r\n";
    std::smatch matches;
    std::regex e("(GET|POST|HEAD|DELETE) ([^?]*)(?:\\?(.*)?) (HTTP/1\\.[01])(?:\r|\r\n)?");
    //(GET|POST|HEAD|DELETE) 表示匹配并提取括号内的字符
    //([^?]*)  [^?] 表示匹配非问号字符 [?]则表示匹配问号字符 * 表示匹配提取非问号字符0次或多次
    //\\?(.*) \\? 表示原始的？ (.*)表示提取后面的字符0次或多次
    //(HTTP/1\\.[01]) 表示提取的字符串是 HTTP/1.1 或 HTTP/1.0就匹配并提取
    //(?:...)表示匹配但是不提取字符 (?:\\?(.*)?) 表示匹配问号开头直到空格的字符，但是不提取， 而问号后面的字符则是有则提取，没有则不提取
    //最后的?表示匹配0次或1次
    bool ret = std::regex_match(s,matches,e);
    if(ret == false)
    {
       std::cout<<"nothing"<<std::endl;
        return -1;
    }
    for(auto & s : matches)
    {
        std::cout<<s<<std::endl;
    }
    return 0;
}
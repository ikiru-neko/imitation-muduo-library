#include "Http.hpp"

#define WWWROOT "./wwwroot"

std::string ResponseStr(const HttpRequest& req)
{
    std::stringstream ss;
    ss << req._version << " " << req._method << " " << req._path << "\r\n";
    for (auto& it : req._headers)
    {
        ss << it.first << ": " << it.second << "\r\n";
    }
    for (auto& it : req._param)
    {
        ss << it.first << ": " << it.second << "\r\n";
    }
    ss << "\r\n";
    ss << req._body << "\r\n";
    return ss.str();
}

void Get(const HttpRequest& req, HttpResponse* rsp)
{
    rsp->SetContent(ResponseStr(req), "text/plain");
}
void Put(const HttpRequest& req, HttpResponse* rsp)
{
    rsp->SetContent(ResponseStr(req), "text/plain");
}
void Post(const HttpRequest& req, HttpResponse* rsp)
{
    rsp->SetContent(ResponseStr(req), "text/plain");
}
void Delete(const HttpRequest& req, HttpResponse* rsp)
{
    rsp->SetContent(ResponseStr(req), "text/plain");
}
int main()
{
    HttpSever httpsever(8080);
    httpsever.SetThreadCount(2);
    httpsever.SetBaseDir(WWWROOT);
    httpsever.Get("/hello", Get);
    httpsever.Put("/Put", Put);
    httpsever.Post("/login", Post);
    httpsever.Delete("/Delete", Delete);
    httpsever.Listen();
    return 0;
}
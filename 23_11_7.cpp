// #include "../Sever.hpp"

// int main()
// {
//     Socket client;
//     // 连接
//     bool ret = client.CreateClient(8080, "127.0.0.1");
//     std::string str = "GET /hello HTTP/1.1\r\nConnection: keep-alive\r\nContent-Length: 0\r\n\r\n";
//     while(1)
//     {
//         assert(client.Send(str.c_str(),str.size()) != -1);
//         char buf[1024];
//         assert(client.Recv(buf,1023));
//         DEBUG_LOG("%s",buf);
//         sleep(3);
//     }
//     client.Close();
//     return 0;
// }
//测试超时连接
// #include "../Sever.hpp"

// int main()
// {
//     Socket client;
//     // 连接
//     bool ret = client.CreateClient(8080, "127.0.0.1");
//     std::string str = "GET /hello HTTP/1.1\r\nConnection: keep-alive\r\nContent-Length: 0\r\n\r\n";
//     while(1)
//     {
//         assert(client.Send(str.c_str(),str.size()) != -1);
//         char buf[1024];
//         assert(client.Recv(buf,1023));
//         DEBUG_LOG("%s",buf);
//         sleep(30);
//     }
//     client.Close();
//     return 0;
// }
//测试正文长度和正文实际长度不符
#include "../Sever.hpp"

int main()
{
    Socket client;
    // 连接
    bool ret = client.CreateClient(8080, "127.0.0.1");
    std::string str = "GET /hello HTTP/1.1\r\nConnection: keep-alive\r\nContent-Length: 100\r\n\r\nhello";
    while (1)
    {
        assert(client.Send(str.c_str(), str.size()) != -1);
        assert(client.Send(str.c_str(), str.size()) != -1);
        assert(client.Send(str.c_str(), str.size()) != -1);
        assert(client.Send(str.c_str(), str.size()) != -1);
        char buf[1024];
        assert(client.Recv(buf, 1023));
        DEBUG_LOG("%s", buf);
        sleep(3);
    }
    client.Close();
    return 0;
}

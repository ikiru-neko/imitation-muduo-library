
using EventCallBack = std::function<void()>;

class Channel
{
private:
    int _fd;
    // 说明对应文件描述符需要监控什么事件
    uint32_t _event;
    // 记录什么事件就绪了
    uint32_t _revent;
    // 读事件回调函数
    EventCallBack _ReadCb;
    // 写事件回调函数
    EventCallBack _WriteCb;
    // 错误事件回调函数
    EventCallBack _ErrorCb;
    // 对方关闭连接的回调函数
    EventCallBack _CloseCb;
    // 任意事件就绪的回调函数
    EventCallBack _EventCb;

public:
    Channel(int fd)
        : _fd(fd), _event(0), _revent(0)
    {
    }

    int Fd()
    {
        return _fd;
    }

    void SetReadcb(const EventCallBack& cb) { _ReadCb = cb; }
    void SetWritecb(const EventCallBack& cb) { _WriteCb = cb; }
    void SetErrorcb(const EventCallBack& cb) { _ErrorCb = cb; }
    void SetClosecb(const EventCallBack& cb) { _CloseCb = cb; }
    void SetEventcb(const EventCallBack& cb) { _EventCb = cb; }
    // 以下的几个函数只是对 _event 进行了操作，实际上还需要把它们放在EventLoop中去
    //  描述符是否监控可读
    bool ReadAble() { return _event & EPOLLIN; }
    //  描述符是否监控可写
    bool WriteAble() { return _event & EPOLLOUT; }
    //  对描述符监控可读
    void EnableRead() { _event &= EPOLLIN; }
    //  对描述符监控可写
    void EnableWrite() { _event &= EPOLLOUT; }
    //  解除可读事件监控
    void DisableRead() { _event &= ~EPOLLIN; }
    //  解除可写事件监控
    void DisableWrite() { _event &= ~EPOLLOUT; }
    //  解除所有事件监控
    void DisableAll() { _event = 0; }
    //  移除所有事件
    void Remove()
    { /*后续需要利用EventLoop模块来将Epoll模型中的节点除掉*/
    }
    // 一旦事件就绪了就调用这个函数
    void HandlerEvent()
    {
        // 若是读事件就绪，就调用读取回调函数
        // 如果是客户端关闭了连接（EPOLLRDHUP）触发也需要将数据读取掉
        // 若是客户端发送了紧急数据，也需要调用读取回调函数
        if (_revent & EPOLLIN || _revent & EPOLLRDHUP || _revent & EPOLLPRI)
        {
            if (_ReadCb)
                _ReadCb();
            if (_EventCb)
                _EventCb();
        }
        // 写，错误，和异常有可能会关闭连接，也有可能重复关闭，
        //  若是写事件就绪就调用写回调函数
        if (_revent & EPOLLOUT)
        {
            if (_WriteCb)
                _WriteCb();
            if (_EventCb) // 放在写事件后面，刷新活跃度
                _EventCb();
        }
        // 若是发生错误，则调用错误回调函数
        else if (_revent & EPOLLERR)
        {
            if (_EventCb)//放在前面，防止连接关闭造成服务器崩溃
                _EventCb();
            if (_ErrorCb)
                _ErrorCb();
        }
        // 若是对方连接异常或者断开就调用关闭函数
        else if (_revent & EPOLLHUP)
        {
            if (_EventCb)
                _EventCb();
            if (_CloseCb)
                _CloseCb();
        }
        // 任意事件就绪都需要调用这个函数
    }
};
#include <string>
#include <iostream>
#include <vector>
#include "../Source/LOG.hpp"
#include <fstream>
using namespace std;

static size_t Split(const string& src, const string sep, vector<string>* array)
{
    size_t offset = 0;
    while (offset < src.size())
    {
        size_t pos = src.find(sep, offset);
        if (pos == std::string::npos)
        {
            array->push_back(src.substr(offset));
            return array->size();
        }
        if (offset == pos)
        {
            offset = pos + sep.size();
            continue;
        }
        array->push_back(src.substr(offset, pos - offset));
        offset = pos + sep.size();
    }
    return array->size();
}
static bool ReadFile(const string& filename, string* buf)
{
    std::ifstream ifs(filename, std::ios::binary);
    if (ifs.is_open() == false)
    {
        ERROR_LOG("FILE OPEN FAILED!!");
        return false;
    }
    ifs.seekg(0, ifs.end);
    size_t fsize = ifs.tellg();
    ifs.seekg(0, ifs.beg);
    buf->resize(fsize);
    ifs.read(&(*buf)[0], fsize);
    if (ifs.good() == false)
    {
        ERROR_LOG("READ %s FILE FAILED!!", filename.c_str());
        ifs.close();
        return false;
    }
    ifs.close();
    return true;
}

static bool WriteFile(const string& filename, const string& buf)
{
    std::ofstream ofs(filename, std::ios::binary | std::ios::trunc);
    if (ofs.is_open() == false)
    {
        ERROR_LOG("FILE OPEN FAILED!!");
        return false;
    }
    ofs.write(buf.c_str(), buf.size());
    if (ofs.good() == false)
    {
        ERROR_LOG("WRITE %s FILE FAILED!!", filename.c_str());
        ofs.close();
        return false;
    }
    ofs.close();
    return true;
}

int main()
{
    string buf("Play Games");
    WriteFile("test.txt", buf);
    // string buf;
    // ReadFile("test.txt", &buf);
    // cout << buf << endl;
    // string str = "abc";
    // vector<string> array;
    // Split(str, ",", &array);
    // for (auto &s : array)
    // {
    //     cout << "[" << s << "]" << endl;
    // }
    return 0;
}
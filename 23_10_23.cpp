#include "../Sever.hpp"

void OnConnected(PtrConnection con)
{
    DEBUG_LOG("NEW CONNECTION : %p", con.get());
}
void OnClosed(PtrConnection con)
{
    DEBUG_LOG("CLOSE CONNECTION : %p", con.get());
}
void OnMessage(PtrConnection con, Buffer* buf)
{
    DEBUG_LOG("%s", buf->ReadPosition());
    // 读取之后就需要将已经读取的数据给向后移动
    buf->MoveReadOffSet(buf->ReadAbleSize());
    string str = "Hello";
    con->Send(str.c_str(), str.size());
}
int main()
{
    TCPSever sever(8080);
    sever.SetCloseCallBack(OnClosed);
    sever.SetConnectCallBack(OnConnected);
    sever.SetMessageCallBack(OnMessage);
    sever.SetThreadCount(2);
    // sever.EnableInactiveRelease(10);
    sever.Start();
    return 0;
}


#include"echo.hpp"

int main()
{
    EchoSever ecs(8080);
    ecs.Start();
    return 0;
}

#include "../Sever.hpp"

class EchoSever
{
private:
    TCPSever _sever;

private:
    void OnConnected(PtrConnection con)
    {
        DEBUG_LOG("NEW CONNECTION : %p", con.get());
    }
    void OnClosed(PtrConnection con)
    {
        DEBUG_LOG("CLOSE CONNECTION : %p", con.get());
    }
    void OnMessage(PtrConnection con, Buffer* buf)
    {
        string str(buf->ReadAsString(buf->ReadAbleSize()));
        buf->MoveReadOffSet(buf->ReadAbleSize());
        con->Send(str.c_str(), str.size());
    }

public:
    EchoSever(int port)
        : _sever(8080)
    {
        _sever.SetConnectCallBack(std::bind(&EchoSever::OnConnected, this, std::placeholders::_1));
        _sever.SetMessageCallBack(std::bind(&EchoSever::OnMessage, this, std::placeholders::_1, std::placeholders::_2));
        _sever.SetCloseCallBack(std::bind(&EchoSever::OnClosed, this, std::placeholders::_1));
        _sever.EnableInactiveRelease(10);
        _sever.SetThreadCount(2);
    }
    void Start()
    {
        _sever.Start();
    }
};
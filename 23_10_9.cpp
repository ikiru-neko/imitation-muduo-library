void HandlerRead()
{
    //1.从socket中读取数据
    char buff[65536];
    int ret = _socket.RecvNonBlock(buff, sizeof(buff) - 1);
    if (ret < 0)
    {
        //小于0表示读取出现错误,但是不能直接关闭连接
        //要先查看是否还有遗留的数据待处理
        return ShutdownInloop();
    }
    //2.有数据就放入Buffer中，再调用message_callback来进行业务逻辑处理
    _in_buffer.WriteAndPush(buff, ret);
    if (_in_buffer.ReadAbleSize() > 0)
    {
        return _msg(shared_from_this(), &_in_buffer);
    }
}
void HandlerWrite()
{
    //触发了写事件后
    //将发送缓冲区的东西通过socket发送出去
    ssize_t ret = _socket.SendNonBlock(_out_buffer.ReadPosition(), _out_buffer.ReadAbleSize());
    if (ret < 0)
    {
        //若是写失败了，就说明对方连接关闭了
        //但是在释放之前，还要查看接收缓冲区中是否还有数据
        if (_in_buffer.ReadAbleSize() > 0)
            _msg(shared_from_this(), &_in_buffer);
        return ReleaseInloop();
    }
    //而若是没写失败,就查看是否是待关闭的状态,或者输出缓冲区是否还有数据
    if (_out_buffer.ReadAbleSize() == 0)
    {
        //关闭写事件监控,防止重复写事件
        _channel.DisableWrite();
        if (_statu == DISCONNECTING)
        {
            //若是待关闭状态
            return ReleaseInloop();
        }
    }
    return;

}
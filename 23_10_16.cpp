class Acceptor
{
private:
    Socket _listen;
    EventLoop* _loop;
    Channel _channel;
    using AcceptorCallBack = std::function<void(int)>;
    AcceptorCallBack _acceptor_cb;

private:
    void HandlerRead()
    {
        int newfd = _listen.Accept();
        if (newfd < 0) // 小于0表示接收失败
            return;
        // 接收到后让Sever来进行后续的操作
        if (_acceptor_cb)
            _acceptor_cb(newfd);
    }
    int CreateSever(int port)
    {
        bool ret = _listen.CreateSever(port);
        assert(ret == true);
        return _listen.Fd();
    }

public:
    Acceptor(EventLoop* loop, int port)
        : _loop(loop), _listen(CreateSever(port)), _channel(loop, _listen.Fd())
    {
        _channel.SetReadcb(std::bind(&Acceptor::HandlerRead, this));
    }
    void SetAcceptorCallBack(AcceptorCallBack cb)
    {
        _acceptor_cb = cb;
    }
    void Listen()
    {
        _channel.EnableRead();
    }
};

uint64_t con_id = 0;
unordered_map<uint64_t, PtrConnection> _cons;
EventLoop loop;
void OnMessage(PtrConnection con, Buffer* buf)
{
    //Connection内部会由EventLoop来调用这个函数
    DEBUG_LOG("%s", buf->ReadPosition());
    //读取之后就需要将已经读取的数据给向后移动
    buf->MoveReadOffSet(buf->ReadAbleSize());
    string str = "Hello";
    con->Send(str.c_str(), str.size());
}
void OnDestroy(PtrConnection con)
{
    _cons.erase(con->Id());
}
void OnConnected(PtrConnection con)
{
    DEBUG_LOG("NEW CONNECTION : %p", con.get());
    _cons.insert(std::make_pair(con_id, con));
}
void NewConnection(int fd)
{
    con_id++;
    PtrConnection con(new Connection(&loop, con_id, fd));
    // cout<<"b"<<endl;
    con->SetMessageCallBack(std::bind(OnMessage, std::placeholders::_1, std::placeholders::_2));
    con->SetSrvCloseCallBack(std::bind(OnDestroy, std::placeholders::_1));
    con->SetConnectCallBack(std::bind(OnConnected, std::placeholders::_1));
    //设置读监控和回调函数
    con->EnableInactiveRelease(10);
    con->Established();
    con->Shutdown();
}

int main()
{
    srand((unsigned int)time(NULL));
    Acceptor a(&loop, 8080);
    a.SetAcceptorCallBack(std::bind(NewConnection, std::placeholders::_1));
    a.Listen();
    while (1)
    {
        loop.Start();
    }
    return 0;
}
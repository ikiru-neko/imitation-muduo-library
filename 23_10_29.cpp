class HttpResponse
{
public:
    // 状态码
    int _statu;
    // 响应的头部字段
    std::unordered_map<std::string, std::string> _hearders;
    // 确认是否重定向
    bool _redirect_flag;
    // 重定向所确定的url
    std::string _redirect_url;
    // 响应的正文
    std::string _body;
public:
    HttpResponse()
        : _redirect_flag(false),
        _statu(200)
    {
    }
    HttpResponse(int statu)
        : _redirect_flag(false),
        _statu(statu)
    {
    }
    // 清空
    void ReSet()
    {
        _statu = 200;
        if (_redirect_flag)
        {
            _redirect_flag = false;
            _redirect_url.clear();
        }
        _body.clear();
        _hearders.clear();
    }
    // 设置头部字段
    void SetHeader(const std::string& key, const std::string& val)
    {
        _hearders.insert(make_pair(key, val));
    }
    // 确认是否有头部字段
    bool HasHeader(const std::string& key)
    {
        auto it = _hearders.find(key);
        if (it != _hearders.end())
            return true;
        return false;
    }
    // 获取对应的头部字段
    std::string GetHeader(const std::string& key)
    {
        auto it = _hearders.find(key);
        if (it != _hearders.end())
            return it->second;
        return "";
    }
    // 设置是否重定向,并且要传递重定向的url
    void SetRedirect(const std::string& redirect_url, int statu = 302)
    {
        _redirect_flag = true;
        _statu = statu;
        _redirect_url = redirect_url;
    }
    // 设置正文
    void SetContent(const std::string& body, std::string type = "text/html")
    {
        _body = body;
        SetHeader("Content-Type", type);
    }
    // 查看是否是短连接
    bool Close()
    {
        if (HasHeader("Connection") == true && GetHeader("Connection") == "keep-alive")
            return true;
        return false;
    }
};
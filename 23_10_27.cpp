class HttpRequest
{
public:
    // 请求方法
    std::string _method;
    // 协议版本
    std::string _version;
    // 资源路径
    std::string _path;
    // 正文
    std::string _body;
    std::smatch _matches;
    // 头部字段
    std::unordered_map<std::string, std::string> _headers;
    // 获取字符串
    std::unordered_map<std::string, std::string> _pagram;

public:
    void ReSet()
    {
        _method.clear();
        _version.clear();
        _path.clear();
        _body.clear();
        std::smatch match;
        _matches.swap(match);
        _headers.clear();
        _pagram.clear();
    }
    void SetHeaders(const std::string& key, const std::string& val)
    {
        _headers.insert(make_pair(key, val));
    }
    bool HasHeader(const std::string& key)
    {
        auto it = _headers.find(key);
        if (it == _headers.end())
            return false;
        return true;
    }
    std::string GetHeader(const std::string& key)
    {
        auto it = _headers.find(key);
        if (it == _headers.end())
            return "";
        else
            return it->second;
    }
    void SetPagram(const std::string& key, const std::string& val)
    {
        _pagram.insert(make_pair(key, val));
    }
    bool HasPagram(const std::string& key)
    {
        auto it = _pagram.find(key);
        if (it == _pagram.end())
            return false;
        return true;
    }
    std::string GetPagram(const std::string& key)
    {
        auto it = _headers.find(key);
        if (it == _headers.end())
            return "";
        else
            return it->second;
    }
    // 判断是否为短连接 是短连接就返回true，否则返回false
    bool IsClose()
    {
        if (HasHeader("Connection") == true && GetHeader("Connection") == "keep-alive")
            return false;
        return true;
    }
    size_t GetContentLength()
    {
        bool ret = HasHeader("Content-Length");
        if (ret == false)
        {
            return 0;
        }
        std::string str = GetHeader("Content-Length");
        return std::stol(str);
    }
};
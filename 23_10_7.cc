class Connection;
typedef enum { DISCONNECTED, CONNECTING, CONNECTED, DISCONNECTING } ConnecStatu;
typedef std::shared_ptr<Connection>  PtrConnnection;
class Connnection
{
private:
    uint64_t _connect_id; // 一个唯一的字符，用来标示唯一的Connection对象
    // 同时也可以用做时间轮的id；
    int _sock_fd;         // 对套接字进行控制的fd
    Buffer _in_buffer;    // 输入缓冲区
    Buffer _out_buffer;   // 输出缓冲区
    Any _contest;         // 一个Any对象，用来记录不同类型协议的上下文
    bool _enable_inactive_release; //默认为false，用来确认是否启动了非活跃连接的销毁功能
    ConnecStatu _statu;
    Socket _socket;       //管理socket
    Channel _channel;     //对套接字的各种操作
    //这些回调并不是针对某一个事件(读写之类的事件)而是针对读事件后的业务处理
    using ConnectCallBack = std::function<void(const PtrConnnection&)>;
    using CloseCallBack = std::function<void(const PtrConnnection&)>;
    using MessageCallBack = std::function<void(const PtrConnnection&, Buffer*)>;
    using AnyEventCallBack = std::function<void(const PtrConnnection&)>;
    ConnectCallBack _connect;
    CloseCallBack _close;
    MessageCallBack _msg;
    AnyEventCallBack _any_event;
public:
    Connection();
    ~Connection();
    void Send();//并不是真正的发送数据，而是将数据放在输出缓冲区中
    void Close(); //并不是真正的关闭连接，而是修改状态
    void EnableInactiveRelease(int sec);//启动非活跃连接销毁,需要设置时间
    void DisableInactiveRelease(int sec);//关闭非活跃连接销毁
    void Upgrade(const Context& context, const ConnectCallBack& con, const CloseCallBack& close, const MessageCallBack& msg, const AnyEventCallBack& anyevent);//更换连接,需要修改上下文，以及各种回调
};

class Solution {
public:
    struct cmp {
        bool operator()(const pair<int, int> a, const pair<int, int> b)
        {
            return a.second < b.second;
        }
    };
    vector<int> rearrangeBarcodes(vector<int>& barcodes) {
        unordered_map<int, int> m;
        for (auto x : barcodes)
        {
            m[x]++;
        }
        priority_queue<pair<int, int>, vector<pair<int, int>>, cmp> q;
        for (auto x : m)
        {
            q.push(x);
        }
        vector<int> ret;
        while (q.size())
        {
            pair<int, int> x = q.top();
            q.pop();
            //如果数组没有数或者这个数的尾部和x不相同
            if (ret.size() == 0 || ret.back() != x.first)
            {
                ret.push_back(x.first);
                x.second--;
                //若 nx 为0表示这个数没有了。就不用再push回去了
                if (x.second != 0)
                {
                    q.push(x);
                }
            }
            //这里表示ret尾部的数据跟x相同
            else {
                if (q.size() < 1) return ret;
                pair<int, int> y = q.top();
                q.pop();
                ret.push_back(y.first);
                y.second--;
                if (y.second != 0)
                {
                    q.push(y);
                }
                if (x.second != 0)
                {
                    q.push(x);
                }
            }
        }
        return ret;
    }
};


class Solution {
public:
    vector<int> rearrangeBarcodes(vector<int>& barcodes) {
        unordered_map<int, int> m;
        int maxVal = 0, maxCount = 0;
        for (auto x : barcodes) {
            if (maxCount < ++m[x])
            {
                maxVal = x;
                maxCount = m[x];
            }
        }
        int n = barcodes.size();
        vector<int> ret(n);
        int index = 0;
        for (int i = 0; i < maxCount; i++)
        {
            ret[index] = maxVal;
            index += 2;
        }
        m.erase(maxVal);
        for (auto [x, y] : m)
        {
            for (int i = 0; i < y; i++)
            {
                if (index >= n) index = 1;
                ret[index] = x;
                index += 2;
            }
        }
        return ret;
    }
};
int main()
{
    vector<int> n = { 1,1,1,2,2,3,3 };
    rearrangeBarcodes(n);
    return 0;
}
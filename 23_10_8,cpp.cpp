class Connection;
class EventLoop;
typedef enum { DISCONNECTED, CONNECTING, CONNECTED, DISCONNECTING } ConnecStatu;
typedef std::shared_ptr<Connection>  PtrConnnection;
class Connnection
{
private:
    uint64_t _con_id; // 一个唯一的字符，用来标示唯一的Connection对象
    // 同时也可以用做时间轮的id；
    int _sock_fd;         // 对套接字进行控制的fd
    Buffer _in_buffer;    // 输入缓冲区
    Buffer _out_buffer;   // 输出缓冲区
    Any _contest;         // 一个Any对象，用来记录不同类型协议的上下文
    bool _enable_inactive_release; //默认为false，用来确认是否启动了非活跃连接的销毁功能
    ConnecStatu _statu;
    Socket _socket;       //管理socket
    Channel _channel;     //对套接字的各种操作
    EventLoop* _loop;
    //这些回调并不是针对某一个事件(读写之类的事件)而是针对读事件后的业务处理
    using ConnectCallBack = std::function<void(const PtrConnnection&)>;
    using CloseCallBack = std::function<void(const PtrConnnection&)>;
    using MessageCallBack = std::function<void(const PtrConnnection&, Buffer*)>;
    using AnyEventCallBack = std::function<void(const PtrConnnection&)>;
    ConnectCallBack _connect;
    CloseCallBack _close;
    MessageCallBack _msg;
    AnyEventCallBack _any_event;

    CloseCallBack _sever_close;//给Sever来关闭Connection对象的函数
private:
    void HandlerRead();
    void HandlerWrite();
    void HandlerClose();
    void HandlerError();
    void HandlerAnyEvent();
    void EstablishedInLoop();//创建后用来设置对应的回调函数，设置读监控之类的操作
    void SendInloop();//真正的发送数据
    void ShutdownInloop();
    void ReleaseInloop();//真正的释放
    void EnableInactiveReleaseInloop(int sec);
    void DisableInactiveReleaseInloop();//关闭非活跃连接销毁
    void UpgradeInloop(const Context& context,
        const ConnectCallBack& con,
        const CloseCallBack& close,
        const MessageCallBack& msg,
        const AnyEventCallBack& anyevent);//更换连接,需要修改上下文，以及各种回调
public:
    Connection(EventLoop* loop, uint64_t con_id, int sock_fd);
    ~Connection();

    int Id();
    int Fd();
    bool Connected();//查看是否处于连接状态
    void SetContext(const Any* any);//设置上下文 连接建立的时候设置
    Any* GetContext();//返回上下文指针,方便外部进行修改
    void Established();
    void SetConnectCallBack(ConnectCallBack& cb);
    void SetCloseCallBack(CloseCallBack& cb);
    void SetMessageCallBack(MessageCallBack& cb);
    void SetAnyEventCallBack(AnyEventCallBack& cb);
    void Send(char* data, size_t len);//并不是真正的发送数据，而是将数据放在输出缓冲区中
    void Shutdown(); //并不是真正的关闭连接，而是修改状态
    void EnableInactiveRelease(int sec);//启动非活跃连接销毁,需要设置时间
    void DisableInactiveRelease();//关闭非活跃连接销毁
    void Upgrade(const Context& context, const ConnectCallBack& con, const CloseCallBack& close, const MessageCallBack& msg, const AnyEventCallBack& anyevent);//更换连接,需要修改上下文，以及各种回调
};
void Dispatcher(HttpRequest& request, HttpResponse* response, Handlers& handlers)
{
    // 通过正则库来对request的资源请求进行匹配
    for (auto& handler : handlers)
    {
        const std::regex& re = handler.first;
        Handler functor = handler.second;
        bool ret = std::regex_match(request._path, request._matches, re);
        //若是匹配失败，就直接continue来循环下一次
        if (ret == false)
            continue;
        //若是成功了，就直接调用functor进行业务处理
        return functor(request, response);
    }
    //最后若是循环结束依旧没有找到，就直接设置404
    response->_statu = 404;
}
// 开始路由，查看是静态请求还是功能性请求
void Route(HttpRequest& request, HttpResponse* response)
{
    // GET HEAD都是静态请求，先默认进行FileHandler
    if (IsFileHandler(request) == true)
    {
        return FileHandler(request, response);
    }
    if (request._method == "GET" || request._method == "HEAD")
    {
        return Dispatcher(request, response, _get_route);
    }
    else if (request._method == "PUT")
    {
        return Dispatcher(request, response, _put_route);
    }
    else if (request._method == "POST")
    {
        return Dispatcher(request, response, _post_route);
    }
    else if (request._method == "DELETE")
    {
        return Dispatcher(request, response, _delete_route);
    }
    // 若是都没有，就修该状态码
    response->_statu = 405; // Method Not Allowed;
}
// 设置上下文
void OnConnected(PtrConnection con)
{
    con->SetContext(HttpContext());
    DEBUG_LOG("NEW CONNECTION:%p", con.get());
}
// 解析并处理请求
void OnMessage(PtrConnection con, Buffer* buf)
{
    while (buf->ReadAbleSize() > 0)
    {
        // 1.获取连接的上下文
        HttpContext* context = con->GetContext()->get<HttpContext>();
        // 2.处理上下文请求，并获得HttpRequest对象
        //   1.有可能请求出错了
        //   2.请求没出错
        context->RecvHttpRequest(buf);
        HttpRequest& request = context->GetRequest();
        HttpResponse response(context->GetResponseStatu());
        if (context->GetResponseStatu() >= 400)
        {
            // 大于等于400说明出现了错误，就关闭连接并返回
            ErrorHandler(request, response);
            WriteResponse(con, request, response);
            context->Reset();
            return;
        }
        // 3.进行路由，并进行业务处理
        Route(request, &response);
        // 4.进行业务处理后，获得HttpResponse对象，并发送过去
        WriteResponse(con, request, response);
        context->Reset();
        // 5.若是短连接，就需要直接shutdown这个连接
        if (request.Close() == false)
            con->Shutdown();
    }
}

void WriteResponse(const PtrConnection con, HttpRequest& request, HttpResponse& response)
{
    // 1.完善头部字段
    //  1.长短连接
    //  2.是否有正文以及正文长度
    // 3.是否有正文以及正文类型
    if (request.Close() == true)
    {
        // true表示是长连接
        if (response.HasHeader("Connection") == false)
        {
            response.SetHeader("Connection", "keep-alive");
        }
    }
    else
    {
        if (response.HasHeader("Connection") == false)
        {
            response.SetHeader("Connection", "close");
        }
    }
    if (response._body.empty() == false && response.HasHeader("Content-Length") == false)
    {
        response.SetHeader("Content-Length", std::to_string(request._body.size()));
    }
    if (response._body.empty() == false && response.HasHeader("Content-Type") == false)
    {
        response.SetHeader("Content-Type", "otect/stream");
    }
    if (response._redirect_flag == true)
    {
        response.SetHeader("Location", response._redirect_url);
    }
    // 2.按照Http协议格式填充字符串
    std::stringstream rep_str;
    rep_str << request._version << " " << response._statu << Util::GetDesc(response._statu) << "\r\n";
    for (auto head : response._hearders)
    {
        rep_str << head.first << ":" << head.second << "\r\n";
    }
    rep_str << "\r\n";
    rep_str << response._body;
    // 3.发送数据
    con->Send(rep_str.str().c_str(), rep_str.str().size());
}
void ErrorHandler(HttpRequest& request, HttpResponse& response)
{
    std::string body;
    body += "<html>";
    body += "<head>";
    body += "<meta http-equiv='Content-Type' content='text/html;charset=utf-8'>";
    body += "</head>";
    body += "<body>";
    body += "<h1>";
    body += std::to_string(response._statu);
    body += " : ";
    body += Util::GetDesc(response._statu);
    body += "</h1>";
    body += "</body>";
    body += "</html>";
    return;
}

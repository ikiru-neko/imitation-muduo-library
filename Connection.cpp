
class Connection;
typedef enum
{
    DISCONNECTED,
    CONNECTING,
    CONNECTED,
    DISCONNECTING
} ConnecStatu;
typedef std::shared_ptr<Connection> PtrConnnection;
class Connection : public enable_shared_from_this<Connection>
{
private:
    uint64_t _con_id;              // 一个唯一的字符，用来标示唯一的Connection对象
    // 同时也可以用做时间轮的id；
    int _sock_fd;                  // 对套接字进行控制的fd
    Buffer _in_buffer;             // 输入缓冲区
    Buffer _out_buffer;            // 输出缓冲区
    Any _context;                  // 一个Any对象，用来记录不同类型协议的上下文
    bool _enable_inactive_release; // 默认为false，用来确认是否启动了非活跃连接的销毁功能
    ConnecStatu _statu;
    EventLoop* _loop;
    Socket _socket;   // 管理socket
    Channel _channel; // 对套接字的各种操作
    // 这些回调并不是针对某一个事件(读写之类的事件)而是针对读事件后的业务处理
    using ConnectCallBack = std::function<void(const PtrConnnection&)>;
    using CloseCallBack = std::function<void(const PtrConnnection&)>;
    using MessageCallBack = std::function<void(const PtrConnnection&, Buffer*)>;
    using AnyEventCallBack = std::function<void(const PtrConnnection&)>;
    ConnectCallBack _connect;
    CloseCallBack _close;
    MessageCallBack _msg;
    AnyEventCallBack _any_event;

    CloseCallBack _sever_close; // 给Sever来关闭Connection对象的函数
private:
    void HandlerRead()
    {
        // 1.从socket中读取数据
        char buff[65536];
        int ret = _socket.RecvNonBlock(buff, sizeof(buff) - 1);
        if (ret < 0)
        {
            // 小于0表示读取出现错误,但是不能直接关闭连接
            // 要先查看是否还有遗留的数据待处理
            return ShutdownInloop();
        }
        // 2.有数据就放入Buffer中，再调用message_callback来进行业务逻辑处理
        _in_buffer.WriteAndPush(buff, ret);
        if (_in_buffer.ReadAbleSize() > 0)
        {
            return _msg(shared_from_this(), &_in_buffer);
        }
    }
    void HandlerWrite()
    {
        // 触发了写事件后
        // 将发送缓冲区的东西通过socket发送出去
        ssize_t ret = _socket.SendNonBlock(_out_buffer.ReadPosition(), _out_buffer.ReadAbleSize());
        if (ret < 0)
        {
            // 若是写失败了，就说明对方连接关闭了
            // 但是在释放之前，还要查看接收缓冲区中是否还有数据
            if (_in_buffer.ReadAbleSize() > 0)
                _msg(shared_from_this(), &_in_buffer);
            return ReleaseInloop();
        }
        // 而若是没写失败,就查看是否是待关闭的状态,或者输出缓冲区是否还有数据
        if (_out_buffer.ReadAbleSize() == 0)
        {
            // 关闭写事件监控,防止重复写事件
            _channel.DisableWrite();
            if (_statu == DISCONNECTING)
            {
                // 若是待关闭状态
                return ReleaseInloop();
            }
        }
        return;
    }
    void HandlerClose()
    {
        if (_in_buffer.ReadAbleSize() > 0)
        {
            _msg(shared_from_this(), &_in_buffer);
        }
        ReleaseInloop();
    }
    void HandlerError() { HandlerClose(); }
    void HandlerAnyEvent()
    {
        if (_enable_inactive_release == true)
        {
            _loop->TimerRefresh(_con_id);
        }
        if (_any_event)
            _any_event(shared_from_this());
    }
    // 创建后用来设置对应的回调函数，设置读监控之类的操作
    void EstablishedInLoop()
    {
        // 1.设置连接状态
        assert(_statu == CONNECTING);
        _statu = CONNECTED;
        // 2.启动读监控
        // 这个操作应该在设置时间轮的定时销毁任务后进行
        // 若是在启动读监控之后再设置定时销毁任务，可能会出现未设置销毁任务时间轮就已经到了的情况
        // 从而导致野指针
        _channel.EnableRead();
        // 3.调用回调函数
        if (_connect)
            _connect(shared_from_this());
    }
    // 真正的释放
    void ReleaseInloop()
    {
        // 1.设置状态
        _statu = DISCONNECTED;
        // 2.移除事件监控
        _channel.Remove();
        // 3.关闭文件描述符
        _socket.Close();
        // 3.5 取消定时器中的销毁任务，防止野指针操作
        if (_loop->IsInWheel(_con_id))
            DisableInactiveReleaseInloop();
        // 4.调用用户设置的close函数
        // 先调用用户的然后再调用服务器的，能够防止connection对象被释放从而导致野指针
        if (_close)
            _close(shared_from_this());
        // 5.调用sever设置的close函数
        if (_sever_close)
            _sever_close(shared_from_this());
    }
    void SendInloop(char* data, size_t len)
    {
        if (_statu == DISCONNECTED)
            return;
        // 将数据放入发送缓冲区
        _in_buffer.WriteAndPush(data, len);
        // 启动写监控
        if (_channel.WriteAble() == false)
            _channel.EnableWrite();
    }
    void ShutdownInloop()
    {
        // 修改状态
        _statu = DISCONNECTING;
        // 判断发送缓冲区和接收缓冲区是否有数据待处理
        // 若是接收缓冲区还有东西，就调用业务处理回调函数
        if (_in_buffer.ReadAbleSize() > 0)
        {
            _msg(shared_from_this(), &_in_buffer);
        }
        // 若是发送缓冲区还有数据，就调用
        if (_out_buffer.ReadAbleSize() > 0)
        {
            if (_channel.WriteAble() == false)
                _channel.EnableWrite();
        }
        // 没有数据就直接释放
        if (_out_buffer.ReadAbleSize() == 0)
            ReleaseInloop();
    }
    // 启动非活跃连接
    void EnableInactiveReleaseInloop(uint32_t sec)
    {
        // 1.设置enable_inactive_release
        _enable_inactive_release = true;
        // 2.添加定时销毁任务
        if (_loop->IsInWheel(_con_id))
            return _loop->TimerRefresh(_con_id); // 若是已经添加了，就刷新
        // 否则就添加
        _loop->TimerAdd(_con_id, sec, std::bind(&Connection::ReleaseInloop, this));
    }
    // 关闭非活跃连接销毁
    void DisableInactiveReleaseInloop()
    {
        _enable_inactive_release = false;
        if (_loop->IsInWheel(_con_id))
            _loop->TimerCancel(_con_id);
    }
    void UpgradeInloop(const Any& context,
        const ConnectCallBack& con,
        const CloseCallBack& close,
        const MessageCallBack& msg,
        const AnyEventCallBack& anyevent) // 更换连接,需要修改上下文，以及各种回调
    {
        _context = context;
        _connect = con;
        _close = close;
        _msg = msg;
        _any_event = anyevent;
    }

public:
    Connection(EventLoop* loop, uint64_t con_id, int sock_fd)
        : _loop(loop), _con_id(con_id), _sock_fd(sock_fd), _channel(_loop, _sock_fd),
        _enable_inactive_release(false), _statu(CONNECTING), _socket(_sock_fd)
    {
    }
    ~Connection() { DEBUG_LOG("RELEASE CONNECTION : %p", this); }
    uint64_t Id() { return _con_id; }
    int Fd() { return _sock_fd; }
    bool Connected() { return _statu == CONNECTED; }                // 查看是否处于连接状态
    void SetContext(const Any* context) {
        _context = context;
    }
    // 返回上下文指针,方便外部进行修改
    Any* GetContext() { return &_context; }
    void Established() { _loop->RunInLoop(std::bind(&Connection::EstablishedInLoop, this)); }
    void SetConnectCallBack(ConnectCallBack& cb) { _connect = cb; }
    void SetCloseCallBack(CloseCallBack& cb) { _close = cb; }
    void SetMessageCallBack(MessageCallBack& cb) { _msg = cb; }
    void SetAnyEventCallBack(AnyEventCallBack& cb) { _any_event = cb; }
    void SetSrvCloseCallBack(CloseCallBack& cb) { _sever_close = cb; }
    void Send(char* data, size_t len) { _loop->RunInLoop(std::bind(&Connection::SendInloop, this, data, len)); }                                                                                                             // 并不是真正的发送数据，而是将数据放在输出缓冲区中
    void Shutdown() { _loop->RunInLoop(std::bind(&Connection::ShutdownInloop, this)); }                                                                                                                                        // 并不是真正的关闭连接，而是修改状态
    void EnableInactiveRelease(int sec) { _loop->RunInLoop(std::bind(&Connection::EnableInactiveReleaseInloop, this, sec)); }                                                                                                                    // 启动非活跃连接销毁,需要设置时间
    void DisableInactiveRelease() { _loop->RunInLoop(std::bind(&Connection::DisableInactiveReleaseInloop, this)); }// 关闭非活跃连接销毁
    //这个需要保证一旦调用就立马启动，因此需要保证在线程中被调用
    //否则可能会出现协议还未更换，但是出现了新的读写任务，导致使用了原协议的函数从而出错                                                                                                              
    void Upgrade(const Any& context, const ConnectCallBack& con, const CloseCallBack& close, const MessageCallBack& msg, const AnyEventCallBack& anyevent)// 更换连接,需要修改上下文，以及各种回调
    {
        _loop->AssertInLoop();
        _loop->RunInLoop(std::bind(&Connection::UpgradeInloop, this, context, con, close, msg, anyevent));
    }
};
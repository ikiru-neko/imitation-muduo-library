typedef enum
{
    Recv_Http_Error, // 请求信息有错误：资源路径无效，无权限访问..
    Recv_Http_Line,  // 收到了请求行，但是未收完全
    Recv_Http_Head,  // 收到了请求头部字段，但是未收完全
    Recv_Http_Body,  // 收到了正文字段，但是未收完全
    Recv_Http_All    // 所有字段都已经收完了
} HttpRecvStatu;
#define MAX_LINE 8192
class HttpContext
{
private:
    int _resp_statu;           // 响应的状态码
    HttpRequest _request;      // 接收的请求
    HttpRecvStatu _recv_statu; // 接收的状态
private:
    bool RecvHttpLine(Buffer* buf)
    {
        if (_recv_statu != Recv_Http_Line)
            return false;
        std::string line = buf->GetOneLine();
        // 两个结果:1.没收到数据，2.一行太长了
        if (line.size() == 0)
        {
            if (buf->ReadAbleSize() > MAX_LINE)
            {
                _resp_statu = 414; // URL TOO LONG
                _recv_statu = Recv_Http_Error;
                return false;
            }
            return true;
        }
        if (line.size() > MAX_LINE)
        {
            _resp_statu = 414; // URL TOO LONG
            _recv_statu = Recv_Http_Error;
            return false;
        }
        bool ret = ParseHttpLine(line);
        if (ret == false)
            return false;
        buf->MoveReadOffSet(line.size());
        _recv_statu = Recv_Http_Head;
        return true;
    }
    bool ParseHttpLine(std::string& line)
    {
        std::smatch matches;
        std::regex e("(GET|HEAD|POST|PUT|DELETE) (([^?]+)(?:\\?(.*?))?) (HTTP/1\\.[01]) (?:\r\n|\n)");
        bool ret = std::regex_match(line, matches, e);
        if (ret == false)
        {
            _recv_statu = Recv_Http_Error;
            _resp_statu = 400; // Bad Request
            return false;
        }
        // 分别获取请求方法，资源路径，Http版本
        // 0号是整个头部字符串
        // 1号是请求方法,2号是请求的资源路径
        // 3号是查询字符串,4号是版本信息
        _request._method = matches[1];
        _request._path = Util::UrlDecode(matches[2], false);
        _request._version = matches[4];
        std::vector<string> qurery_string_array;
        std::string qurery_string = matches[3];
        Util::Split(qurery_string, "&", &qurery_string_array);
        for (auto& str : qurery_string_array)
        {
            int pos = str.find("=");
            if (pos == std::string::npos)
            {
                _recv_statu = Recv_Http_Error;
                _resp_statu = 400; // Bad Request
                return false;
            }
            std::string key = Util::UrlDecode(str.substr(0, pos), true);
            std::string val = Util::UrlDecode(str.substr(pos + 1), true);
            _request.SetParam(key, val); // 设置查询字符串
        }
        return true;
    }
    bool RecvHttpHead(Buffer* buf)
    {
        // 没收完Line就返回false
        if (_recv_statu != Recv_Http_Head)
        {
            return false;
        }
        while (true)
        {
            std::string line = buf->GetOneLine();
            // 两个结果:1.没收到数据，2.一行太长了
            if (line.size() == 0)
            {
                if (buf->ReadAbleSize() > MAX_LINE)
                {
                    _resp_statu = 414; // URL TOO LONG
                    _recv_statu = Recv_Http_Error;
                    return false;
                }
                return true;
            }
            if (line.size() > MAX_LINE)
            {
                _resp_statu = 414; // URL TOO LONG
                _recv_statu = Recv_Http_Error;
                return false;
            }
            buf->MoveReadOffSet(line.size());
            if (line == "\n" || line == "\r\n")
                break;
            bool ret = ParseHttpHead(line);
            if (ret == false)
            {
                return false;
            }
        }
        // 表示头部已经获取完毕
        _recv_statu = Recv_Http_Body;
        return true;
    }
    bool ParseHttpHead(std::string& line)
    {
        // key: val的格式
        int pos = line.find(": ");
        if (pos == std::string::npos)
        {
            _resp_statu = 400; // Bad Request
            _recv_statu = Recv_Http_Head;
            return false;
        }
        std::string key = line.substr(0, pos);
        std::string val = line.substr(pos + 2);
        _request.SetHeaders(key, val);
        return false;
    }
    bool RecvHttpBody(Buffer* buf)
    {
        // 1.获取正文长度
        if (_recv_statu != Recv_Http_Body)
            return false;
        size_t content_length = _request.GetContentLength();
        if (content_length == 0)
        {
            _recv_statu = Recv_Http_All;
            return true;
        }
        size_t real_len = content_length - _request._body.size();
        // 2.对比buf中的正文长度和真正需要的正文长度
        if (real_len <= buf->ReadAbleSize())
        {
            _request._body.append(buf->ReadPosition(), real_len);
            buf->MoveReadOffSet(real_len);
            _recv_statu = Recv_Http_All;
            return true;
        }
        // 3.1 数据过多 3.2 数据过少
        // 数据太少了则将数据全部取出来，但是不修改Recv_statu
        _request._body.append(buf->ReadPosition(), buf->ReadAbleSize());
        buf->MoveReadOffSet(buf->ReadAbleSize());
        return true;
    }

public:
    HttpContext() : _resp_statu(200), _recv_statu(Recv_Http_Line) {}
    int GetResponseStatu() { return _resp_statu; }       // 获取响应装态码
    HttpRequest& GetRequest() { return _request; }       // 获取接收完毕并已经解析完的请求信息
    HttpRecvStatu GetRecvStatu() { return _recv_statu; } // 获取当前接收的状态
    // 开始接收，通过调用私有函数来进行接收
    void RecvHttpRequest(Buffer* buf)
    {
        switch (_recv_statu)
        {
        case Recv_Http_Line: RecvHttpLine(buf);
        case Recv_Http_Head: RecvHttpHead(buf);
        case Recv_Http_Body: RecvHttpBody(buf);
        }
        return;
    }
};

class HttpSever {
private:
    using Handler = std::function<void(const HttpRequest&, HttpResponse*)>;
    std::unordered_map<std::string, Handler> _get_route;
    std::unordered_map<std::string, Handler> _post_route;
    std::unordered_map<std::string, Handler> _put_route;
    std::unordered_map<std::string, Handler> _delete_route;
    TCPSever _server;
    std::string _basedir;
private:
    void FileHandler();//静态请求
    void Dispatcher();//功能性请求，根据不同的请求来使用不同的路由表
    void Route();//开始路由，查看是静态请求还是功能性请求
    void OnConnected();//设置上下文
    void OnMessage();//解析并处理请求
public:
    //设置对应请求的路由表
    //其中路由表所记录的函数名并不是单纯的一个函数名，而是一个正则表达式
    void Get(std::string& pattern, Handler handler);
    void Post(std::string& pattern, Handler handler);
    void Put(std::string& pattern, Handler handler);
    void Delete(std::string& pattern, Handler handler);
    //设置线程个数
    void SetThreadCount(int count);
    //启动非活跃销毁
    void EnableInactiveRelease(int timeout);
    //启动服务器
    void Listen();
};
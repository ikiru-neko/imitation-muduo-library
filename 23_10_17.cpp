uint64_t con_id = 0;
unordered_map<uint64_t, PtrConnection> _cons;
EventLoop baseloop;
vector<LoopThread> _threads(2);
int idx = 0;
void OnMessage(PtrConnection con, Buffer* buf)
{
    // Connection内部会由EventLoop来调用这个函数
    DEBUG_LOG("%s", buf->ReadPosition());
    // 读取之后就需要将已经读取的数据给向后移动
    buf->MoveReadOffSet(buf->ReadAbleSize());
    string str = "Hello";
    con->Send(str.c_str(), str.size());
}
void OnDestroy(PtrConnection con)
{
    _cons.erase(con->Id());
}
void OnConnected(PtrConnection con)
{
    DEBUG_LOG("NEW CONNECTION : %p", con.get());
    _cons.insert(std::make_pair(con_id, con));
}
void NewConnection(int fd)
{
    con_id++;
    PtrConnection con(new Connection(_threads[idx].GetEventLoop(), con_id, fd));
    idx = (idx + 1) % 2;
    con->SetMessageCallBack(std::bind(OnMessage, std::placeholders::_1, std::placeholders::_2));
    con->SetSrvCloseCallBack(std::bind(OnDestroy, std::placeholders::_1));
    // con->SetConnectCallBack(std::bind(OnConnected, std::placeholders::_1));
    // 设置读监控和回调函数
    con->EnableInactiveRelease(10);
    con->Established();
    DEBUG_LOG("NEW CONNECTION");
    //目前找到的问题 : 在NEWCONNECTION函数中创建的con没有及时让_cons保存，导致出了NewConnection函数
    //con就被销毁了，导致后面的野指针问题
    // con->Shutdown();
}

int main()
{
    Acceptor a(&baseloop, 8080);
    a.SetAcceptorCallBack(std::bind(NewConnection, std::placeholders::_1));
    a.Listen();
    baseloop.Start();
    return 0;
}

class LoopThreadPool
{
private:
    int _thread_count;
    int _next_loop;        // 采用RR轮制来选择运行的Loop，这是指向的下标
    EventLoop* _base_loop; // 主线程运行的基础Loop，没有从属线程就让它来运行
    std::vector<LoopThread*> _threads;
    std::vector<EventLoop*> _loops;

public:
    LoopThreadPool(EventLoop* base_loop) : _base_loop(base_loop), _thread_count(0), _next_loop(0) {}
    void SetThreadCount(int count) { _thread_count = count; } // 设置从属线程的个数
    // 创建从属线程
    void Create()
    {
        if (_thread_count > 0)
        {
            _threads.resize(_thread_count);
            _loops.resize(_thread_count);
            for (int i = 0; i < _thread_count; i++)
            {
                _threads[i] = new LoopThread();
                _loops[i] = _threads[i]->GetEventLoop();
            }
        }
        return;
    }
    // 获取该线程对应的EventLoop
    EventLoop* GetEventLoop()
    {
        if (_thread_count == 0)  return _base_loop;
        _next_loop = (_next_loop + 1) % _thread_count;
        return _loops[_next_loop];
    }
};
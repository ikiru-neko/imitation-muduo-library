class EventLoop
{
private:
    using Functor = std::function<void()>;
    int _event_fd;                    // 用于事件通知的文件描述符
    Epoller _poller;                  // 用于进行事件监控的poller
    std::unique_ptr<Channel> _channel;                // 给_event_fd设置对应的回调函数
    std::vector<Functor> _task_queue; // 任务池，该线程对应的任务都放在这里
    std::thread::id _id;              // 该EventLoop对应的线程id
    std::mutex _mutex;                // 互斥锁，保证任务池的线程安全
private:
    void RunAllTask() // 执行所有任务
    {
        std::vector<Functor> functor;
        {
            // 限定作用域，保证_task_queue的线程安全
            std::unique_lock<std::mutex> lock(_mutex);
            _task_queue.swap(functor);
        }
        for (auto& t : functor)
        {
            t();
        }
        return;
    }
    // 创建一个event_fd
    static int Create_Eventfd()
    {
        int efd = eventfd(0, EFD_CLOEXEC | EFD_NONBLOCK);
        if (efd < 0)
        {
            ERROR_LOG("CREATE EVENTFD FAILE!");
            abort();
        }
        return efd;
    }
    void Read_Event_Fd()
    {
        uint64_t out;
        int ret = read(_event_fd, &out, sizeof(out));
        if (ret < 0)
        {
            if (errno == EAGAIN || errno == EINTR)
            {
                return;
            }
            ERROR_LOG("EVENTFD READ FAILED");
            abort();
        }
        return;
    }
    void WakeUpEvent()
    {
        uint64_t in = 1;
        int ret = write(_event_fd, &in, sizeof(in));
        if (ret < 0)
        {
            if (errno == EINTR)
            {
                return;
            }
            ERROR_LOG("EVENTFD WRITE FAILED");
            abort();
        }
    }

public:
    EventLoop() : _id(std::this_thread::get_id()),
        _event_fd(Create_Eventfd()),
        _channel(new Channel(this, _event_fd))
    {
        // 给_event_fd 设置可读
        _channel->SetReadcb(std::bind(&EventLoop::Read_Event_Fd, this));
        _channel->EnableRead();
    }
    void Start() // 启动EventLoop:1：进行事件监控 2：事件就绪压入队列 3：执行任务
    {
        // 启动事件监控
        std::vector<Channel*> actives;
        _poller.Poll(&actives);
        // 事件就绪压入队列
        for (auto& t : actives)
        {
            t->HandlerEvent();
        }
        RunAllTask();
    }
    void RunInLoop(Functor& cb) // 是对应的EventLoop的任务，就执行，不是就放入对应的任务池中
    {
        if (IsInLoop())
        {
            cb();
            return;
        }
        QueueInLoop(cb);
    }
    void QueueInLoop(Functor& cb) // 将任务压入任务池中
    {
        {
            std::unique_lock<std::mutex> lock(_mutex);
            _task_queue.push_back(cb);
        }
        // 防止阻塞在Poll中，需要手动给eventfd设置写入数据，来唤醒Poll
        WakeUpEvent();
    }
    bool IsInLoop() // 用来判断是否在对应的线程中
    {
        return _id == std::this_thread::get_id();
    }
    void UpdateEpoll(Channel* channel) // 修改或者添加事件监控
    {
        _poller.UpdateEpoll(channel);
    }
    void RemoveEpoll(Channel* channel) // 删除事件监控
    {
        _poller.RemoveEpoll(channel);
    }
};

void Channel::Remove() { _loop->RemoveEpoll(this); }
void Channel::Update() { _loop->UpdateEpoll(this); }
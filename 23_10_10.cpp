void EstablishedInLoop()
{
    // 1.设置连接状态
    assert(_statu == CONNECTING);
    _statu = CONNECTED;
    // 2.启动读监控
    // 这个操作应该在设置时间轮的定时销毁任务后进行
    // 若是在启动读监控之后再设置定时销毁任务，可能会出现未设置销毁任务时间轮就已经到了的情况
    // 从而导致野指针
    _channel.EnableRead();
    // 3.调用回调函数
    if (_connect)
        _connect(shared_from_this());
}
// 真正的释放
void ReleaseInloop()
{
    // 1.设置状态
    _statu = DISCONNECTED;
    // 2.移除事件监控
    _channel.Remove();
    // 3.关闭文件描述符
    _socket.Close();
    // 3.5 取消定时器中的销毁任务，防止野指针操作
    if (_loop->IsInWheel(_con_id))
        DisableInactiveReleaseInloop();
    // 4.调用用户设置的close函数
    // 先调用用户的然后再调用服务器的，能够防止connection对象被释放从而导致野指针
    if (_close)
        _close(shared_from_this());
    // 5.调用sever设置的close函数
    if (_sever_close)
        _sever_close(shared_from_this());
}
void SendInloop(char* data, size_t len)
{
    if (_statu == DISCONNECTED)
        return;
    // 将数据放入发送缓冲区
    _in_buffer.WriteAndPush(data, len);
    // 启动写监控
    if (_channel.WriteAble() == false)
        _channel.EnableWrite();
}
void ShutdownInloop()
{
    // 修改状态
    _statu = DISCONNECTING;
    // 判断发送缓冲区和接收缓冲区是否有数据待处理
    // 若是接收缓冲区还有东西，就调用业务处理回调函数
    if (_in_buffer.ReadAbleSize() > 0)
    {
        _msg(shared_from_this(), &_in_buffer);
    }
    // 若是发送缓冲区还有数据，就调用
    if (_out_buffer.ReadAbleSize() > 0)
    {
        if (_channel.WriteAble() == false)
            _channel.EnableWrite();
    }
    // 没有数据就直接释放
    if (_out_buffer.ReadAbleSize() == 0)
        ReleaseInloop();
}
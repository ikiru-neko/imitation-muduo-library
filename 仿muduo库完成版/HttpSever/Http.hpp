#include "../Source/Sever.hpp"
#include <fstream>
#include <unordered_map>
#include <algorithm>
#include <cassert>
#include <sys/stat.h>
#include <regex>
#include <iostream>

#define DEFAULT_TIMEOUT 10
class Util
{
public:
    // 分割字符串
    // 将src的字符串以sep为分隔符分成n个字符串，全部放在array中
    static size_t Split(const string &src, const string sep, vector<string> *array)
    {
        size_t offset = 0;
        while (offset < src.size())
        {
            size_t pos = src.find(sep, offset);
            if (pos == std::string::npos)
            {
                array->push_back(src.substr(offset));
                return array->size();
            }
            if (offset == pos)
            {
                offset = pos + sep.size();
                continue;
            }
            array->push_back(src.substr(offset, pos - offset));
            offset = pos + sep.size();
        }
        return array->size();
    }
    // 读取文件信息
    static bool ReadFile(const string &filename, string *buf)
    {
        std::ifstream ifs(filename, std::ios::binary);
        if (ifs.is_open() == false)
        {
            ERROR_LOG("FILE OPEN FAILED!!");
            return false;
        }
        ifs.seekg(0, ifs.end);
        size_t fsize = ifs.tellg();
        ifs.seekg(0, ifs.beg);
        buf->resize(fsize);
        ifs.read(&(*buf)[0], fsize);
        if (ifs.good() == false)
        {
            ERROR_LOG("READ %s FILE FAILED!!", filename.c_str());
            ifs.close();
            return false;
        }
        ifs.close();
        return true;
    }
    // 写入文件信息
    static bool WriteFile(const string &filename, const string &buf)
    {
        std::ofstream ofs(filename, std::ios::binary | std::ios::trunc);
        if (ofs.is_open() == false)
        {
            ERROR_LOG("FILE OPEN FAILED!!");
            return false;
        }
        ofs.write(buf.c_str(), buf.size());
        if (ofs.good() == false)
        {
            ERROR_LOG("WRITE %s FILE FAILED!!", filename.c_str());
            ofs.close();
            return false;
        }
        ofs.close();
        return true;
    }
    // URL编码
    // 绝对不编码字符 . - _ ~以及字母和数字
    // 以及空格有需要需要转换为+
    static string UrlEncode(std::string url, bool turn_space_to_plus)
    {
        std::string ret;
        for (auto &s : url)
        {
            if (s == '.' || s == '-' || s == '_' || s == '~' || isalnum(s))
            {
                ret += s;
                continue;
            }
            if (s == ' ' && turn_space_to_plus == true)
            {
                ret += '+';
                continue;
            }
            char tmp[4] = {0};
            // 若是其他的就需要转换为%HH的格式
            snprintf(tmp, 4, "%%%02X", s);
            ret += tmp;
        }
        return ret;
    }
    static char HexToI(char c)
    {
        if (c >= '0' && c <= '9')
        {
            return c - '0';
        }
        else if (c >= 'a' && c <= 'x')
        {
            return c - 'a' + 10;
        }
        else if (c >= 'A' && c <= 'X')
        {
            return c - 'A' + 10;
        }
        return -1;
    }
    static string UrlDecode(const string url, bool turn_plus_to_space)
    {
        string ret;
        for (int i = 0; i < url.size(); i++)
        {
            if (url[i] == '+' && turn_plus_to_space == true)
            {
                ret += url[i];
                continue;
            }
            if (url[i] == '%' && i + 2 < url.size())
            {
                char v1 = HexToI(url[i + 1]);
                char v2 = HexToI(url[i + 2]);
                char v = (v1 << 4) + v2;
                ret += v;
                i += 2;
                continue;
            }
            ret += url[i];
        }
        return ret;
    }
    // 通过状态码获取描述信息
    static string GetDesc(int statu)
    {
        std::unordered_map<int, std::string> _statu_msg = {
            {100, "Continue"},
            {101, "Switching Protocol"},
            {102, "Processing"},
            {103, "Early Hints"},
            {200, "OK"},
            {201, "Created"},
            {202, "Accepted"},
            {203, "Non-Authoritative Information"},
            {204, "No Content"},
            {205, "Reset Content"},
            {206, "Partial Content"},
            {207, "Multi-Status"},
            {208, "Already Reported"},
            {226, "IM Used"},
            {300, "Multiple Choice"},
            {301, "Moved Permanently"},
            {302, "Found"},
            {303, "See Other"},
            {304, "Not Modified"},
            {305, "Use Proxy"},
            {306, "unused"},
            {307, "Temporary Redirect"},
            {308, "Permanent Redirect"},
            {400, "Bad Request"},
            {401, "Unauthorized"},
            {402, "Payment Required"},
            {403, "Forbidden"},
            {404, "Not Found"},
            {405, "Method Not Allowed"},
            {406, "Not Acceptable"},
            {407, "Proxy Authentication Required"},
            {408, "Request Timeout"},
            {409, "Conflict"},
            {410, "Gone"},
            {411, "Length Required"},
            {412, "Precondition Failed"},
            {413, "Payload Too Large"},
            {414, "URI Too Long"},
            {415, "Unsupported Media Type"},
            {416, "Range Not Satisfiable"},
            {417, "Expectation Failed"},
            {418, "I'm a teapot"},
            {421, "Misdirected Request"},
            {422, "Unprocessable Entity"},
            {423, "Locked"},
            {424, "Failed Dependency"},
            {425, "Too Early"},
            {426, "Upgrade Required"},
            {428, "Precondition Required"},
            {429, "Too Many Requests"},
            {431, "Request Header Fields Too Large"},
            {451, "Unavailable For Legal Reasons"},
            {501, "Not Implemented"},
            {502, "Bad Gateway"},
            {503, "Service Unavailable"},
            {504, "Gateway Timeout"},
            {505, "HTTP Version Not Supported"},
            {506, "Variant Also Negotiates"},
            {507, "Insufficient Storage"},
            {508, "Loop Detected"},
            {510, "Not Extended"},
            {511, "Network Authentication Required"}};
        auto ret = _statu_msg.find(statu);
        if (ret == _statu_msg.end())
        {
            return "Unkown";
        }
        return ret->second;
    }
    // 根据文件后缀获取描述信息
    static string ExcMine(string str)
    {
        std::unordered_map<std::string, std::string> _mime_msg = {
            {".aac", "audio/aac"},
            {".abw", "application/x-abiword"},
            {".arc", "application/x-freearc"},
            {".avi", "video/x-msvideo"},
            {".azw", "application/vnd.amazon.ebook"},
            {".bin", "application/octet-stream"},
            {".bmp", "image/bmp"},
            {".bz", "application/x-bzip"},
            {".bz2", "application/x-bzip2"},
            {".csh", "application/x-csh"},
            {".css", "text/css"},
            {".csv", "text/csv"},
            {".doc", "application/msword"},
            {".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
            {".eot", "application/vnd.ms-fontobject"},
            {".epub", "application/epub+zip"},
            {".gif", "image/gif"},
            {".htm", "text/html"},
            {".html", "text/html"},
            {".ico", "image/vnd.microsoft.icon"},
            {".ics", "text/calendar"},
            {".jar", "application/java-archive"},
            {".jpeg", "image/jpeg"},
            {".jpg", "image/jpeg"},
            {".js", "text/javascript"},
            {".json", "application/json"},
            {".jsonld", "application/ld+json"},
            {".mid", "audio/midi"},
            {".midi", "audio/x-midi"},
            {".mjs", "text/javascript"},
            {".mp3", "audio/mpeg"},
            {".mpeg", "video/mpeg"},
            {".mpkg", "application/vnd.apple.installer+xml"},
            {".odp", "application/vnd.oasis.opendocument.presentation"},
            {".ods", "application/vnd.oasis.opendocument.spreadsheet"},
            {".odt", "application/vnd.oasis.opendocument.text"},
            {".oga", "audio/ogg"},
            {".ogv", "video/ogg"},
            {".ogx", "application/ogg"},
            {".otf", "font/otf"},
            {".png", "image/png"},
            {".pdf", "application/pdf"},
            {".ppt", "application/vnd.ms-powerpoint"},
            {".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"},
            {".rar", "application/x-rar-compressed"},
            {".rtf", "application/rtf"},
            {".sh", "application/x-sh"},
            {".svg", "image/svg+xml"},
            {".swf", "application/x-shockwave-flash"},
            {".tar", "application/x-tar"},
            {".tif", "image/tiff"},
            {".tiff", "image/tiff"},
            {".ttf", "font/ttf"},
            {".txt", "text/plain"},
            {".vsd", "application/vnd.visio"},
            {".wav", "audio/wav"},
            {".weba", "audio/webm"},
            {".webm", "video/webm"},
            {".webp", "image/webp"},
            {".woff", "font/woff"},
            {".woff2", "font/woff2"},
            {".xhtml", "application/xhtml+xml"},
            {".xls", "application/vnd.ms-excel"},
            {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
            {".xml", "application/xml"},
            {".xul", "application/vnd.mozilla.xul+xml"},
            {".zip", "application/zip"},
            {".3gp", "video/3gpp"},
            {".3g2", "video/3gpp2"},
            {".7z", "application/x-7z-compressed"}};
        size_t pos = str.find_last_of('.');
        if (pos == string::npos)
        {
            return "application/otect-stream";
        }
        auto it = _mime_msg.find(str.substr(pos));
        if (it == _mime_msg.end())
        {
            return "application/otect-stream";
        }
        return it->second;
    }
    // 判断一个文件是否是目录
    static bool IsDirectory(const string &filename)
    {
        struct stat st;
        int ret = stat(filename.c_str(), &st);
        if (ret < 0)
        {
            return false;
        }
        return S_ISDIR(st.st_mode);
    }
    // 判断一个文件是否是普通文件
    static bool IsRegular(const string &filename)
    {
        struct stat st;
        int ret = stat(filename.c_str(), &st);
        if (ret < 0)
        {
            return false;
        }
        return S_ISREG(st.st_mode);
    }
    // 判断文件路径是否有效
    static bool IsValidPath(const string &filename)
    {
        std::vector<string> array;
        Split(filename, "/", &array);
        int level = 0;
        for (auto &t : array)
        {
            DEBUG_LOG("level : %d",level);
            if (t == "..")
            {
                level--;
                if (level < 0)
                {
                 return false;
                }
                continue;
            }
            level++;
        }

        return true;
    }
};

class HttpRequest
{
public:
    // 请求方法
    std::string _method;
    // 协议版本
    std::string _version;
    // 资源路径
    std::string _path;
    // 正文
    std::string _body;
    std::smatch _matches;
    // 头部字段
    std::unordered_map<std::string, std::string> _headers;
    // 获取字符串
    std::unordered_map<std::string, std::string> _param;

public:
    HttpRequest() : _version("HTTP/1.1")
    {
    }
    void ReSet()
    {
        _method.clear();
        _version = "HTTP/1.1";
        _path.clear();
        _body.clear();
        std::smatch match;
        _matches.swap(match);
        _headers.clear();
        _param.clear();
    }
    void SetHeaders(const std::string &key, const std::string &val)
    {
        _headers.insert(make_pair(key, val));
    }
    bool HasHeader(const std::string &key)
    {
        auto it = _headers.find(key);
        if (it == _headers.end())
            return false;
        return true;
    }
    std::string GetHeader(const std::string &key)
    {
        auto it = _headers.find(key);
        if (it == _headers.end())
            return "";
        else
            return it->second;
    }
    void SetParam(const std::string &key, const std::string &val)
    {
        _param.insert(make_pair(key, val));
    }
    bool HasParam(const std::string &key)
    {
        auto it = _param.find(key);
        if (it == _param.end())
            return false;
        return true;
    }
    std::string GetPagram(const std::string &key)
    {
        auto it = _headers.find(key);
        if (it == _headers.end())
            return "";
        else
            return it->second;
    }
    // 判断是否为短连接 是短连接就返回false，否则返回true
    bool Close()
    {
        // 是长连接就放回true
        if (HasHeader("Connection") == true && GetHeader("Connection") == "keep-alive")
            return false;
        return true;
    }
    size_t GetContentLength()
    {
        bool ret = HasHeader("Content-Length");
        if (ret == false)
        {
            return 0;
        }
        std::string str = GetHeader("Content-Length");
        return std::stol(str);
    }
};
class HttpResponse
{
public:
    // 状态码
    int _statu;
    // 响应的头部字段
    std::unordered_map<std::string, std::string> _hearders;
    // 确认是否重定向
    bool _redirect_flag;
    // 重定向所确定的url
    std::string _redirect_url;
    // 响应的正文
    std::string _body;

public:
    HttpResponse()
        : _redirect_flag(false),
          _statu(200)
    {
    }
    HttpResponse(int statu)
        : _redirect_flag(false),
          _statu(statu)
    {
    }
    // 清空
    void ReSet()
    {
        _statu = 200;
        if (_redirect_flag)
        {
            _redirect_flag = false;
            _redirect_url.clear();
        }
        _body.clear();
        _hearders.clear();
    }
    // 设置头部字段
    void SetHeader(const std::string &key, const std::string &val)
    {
        _hearders.insert(make_pair(key, val));
    }
    // 确认是否有头部字段
    bool HasHeader(const std::string &key)
    {
        auto it = _hearders.find(key);
        if (it != _hearders.end())
            return true;
        return false;
    }
    // 获取对应的头部字段
    std::string GetHeader(const std::string &key)
    {
        auto it = _hearders.find(key);
        if (it != _hearders.end())
            return it->second;
        return "";
    }
    // 设置是否重定向,并且要传递重定向的url
    void SetRedirect(const std::string &redirect_url, int statu = 302)
    {
        _redirect_flag = true;
        _statu = statu;
        _redirect_url = redirect_url;
    }
    // 设置正文
    void SetContent(const std::string &body, std::string type = "text/html")
    {
        _body = body;
        SetHeader("Content-Type", type);
    }
    // 查看是否是短连接
    bool Close()
    {
        if (HasHeader("Connection") == true && GetHeader("Connection") == "keep-alive")
            return false;
        return true;
    }
};

typedef enum
{
    Recv_Http_Error, // 请求信息有错误：资源路径无效，无权限访问..
    Recv_Http_Line,  // 收到了请求行，但是未收完全
    Recv_Http_Head,  // 收到了请求头部字段，但是未收完全
    Recv_Http_Body,  // 收到了正文字段，但是未收完全
    Recv_Http_All    // 所有字段都已经收完了
} HttpRecvStatu;
#define MAX_LINE 8192
class HttpContext
{
private:
    int _resp_statu;           // 响应的状态码
    HttpRequest _request;      // 接收的请求
    HttpRecvStatu _recv_statu; // 接收的状态
private:
    bool RecvHttpLine(Buffer *buf)
    {
        if (_recv_statu != Recv_Http_Line)
            return false;
        std::string line = buf->GetOneLineAndPop();
        // 两个结果:1.没收到数据，2.一行太长了
        if (line.size() == 0)
        {
            if (buf->ReadAbleSize() > MAX_LINE)
            {
                _resp_statu = 414; // URL TOO LONG
                _recv_statu = Recv_Http_Error;
                return false;
            }
            return true;
        }
        if (line.size() > MAX_LINE)
        {
            _resp_statu = 414; // URL TOO LONG
            _recv_statu = Recv_Http_Error;
            return false;
        }
        bool ret = ParseHttpLine(line);
        if (ret == false)
        {
            return false;
        }
        _recv_statu = Recv_Http_Head;
        return true;
    }
    bool ParseHttpLine(std::string &line)
    {
        std::smatch matches;
        std::regex e("(GET|HEAD|POST|PUT|DELETE) ([^?]*)(?:\\?(.*))? (HTTP/1\\.[01])(?:\n|\r\n)?", std::regex::icase);
        bool ret = std::regex_match(line, matches, e);
        if (ret == false)
        {
            _recv_statu = Recv_Http_Error;
            _resp_statu = 400; // Bad Request
            return false;
        }
        // 分别获取请求方法，资源路径，Http版本
        // 0号是整个头部字符串
        // 1号是请求方法,2号是请求的资源路径
        // 3号是查询字符串,4号是版本信息
        _request._method = matches[1];
        std::transform(_request._method.begin(), _request._method.end(), _request._method.begin(), ::toupper);
        _request._path = Util::UrlDecode(matches[2], false);
        _request._version = matches[4];
        std::vector<string> qurery_string_array;
        std::string qurery_string = matches[3];
        Util::Split(qurery_string, "&", &qurery_string_array);
        for (auto &str : qurery_string_array)
        {
            int pos = str.find("=");
            if (pos == std::string::npos)
            {
                _recv_statu = Recv_Http_Error;
                _resp_statu = 400; // Bad Request
                return false;
            }
            std::string key = Util::UrlDecode(str.substr(0, pos), true);
            std::string val = Util::UrlDecode(str.substr(pos + 1), true);
            _request.SetParam(key, val); // 设置查询字符串
        }
        return true;
    }
    bool RecvHttpHead(Buffer *buf)
    {
        // 没收完Line就返回false
        if (_recv_statu != Recv_Http_Head)
        {
            return false;
        }
        while (true)
        {
            std::string line = buf->GetOneLine();
            // 两个结果:1.没收到数据，2.一行太长了
            if (line.size() == 0)
            {
                if (buf->ReadAbleSize() > MAX_LINE)
                {
                    _resp_statu = 414; // URL TOO LONG
                    _recv_statu = Recv_Http_Error;
                    return false;
                }
                return true;
            }
            if (line.size() > MAX_LINE)
            {
                _resp_statu = 414; // URL TOO LONG
                _recv_statu = Recv_Http_Error;
                return false;
            }
            buf->MoveReadOffSet(line.size());
            if (line == "\n" || line == "\r\n")
                break;
            bool ret = ParseHttpHead(line);
            if (ret == false)
            {
                return false;
            }
        }
        // 表示头部已经获取完毕
        _recv_statu = Recv_Http_Body;
        return true;
    }
    bool ParseHttpHead(std::string &line)
    {
        // key: val的格式
        if(line.back() == '\n') line.pop_back();
        if(line.back() == '\r') line.pop_back();
        int pos = line.find(": ");
        if (pos == std::string::npos)
        {
            _resp_statu = 400; // Bad Request
            _recv_statu = Recv_Http_Head;
            return false;
        }
        std::string key = line.substr(0, pos);
        std::string val = line.substr(pos + 2);
        _request.SetHeaders(key, val);
        return true;
    }
    bool RecvHttpBody(Buffer *buf)
    {
        // 1.获取正文长度
        if (_recv_statu != Recv_Http_Body)
        {
            return false;
        }
        size_t content_length = _request.GetContentLength();
        if (content_length == 0)
        {
            _recv_statu = Recv_Http_All;
            return true;
        }
        size_t real_len = content_length - _request._body.size();
        // 2.对比buf中的正文长度和真正需要的正文长度
        if (real_len <= buf->ReadAbleSize())
        {
            _request._body.append(buf->ReadPosition(), real_len);
            buf->MoveReadOffSet(real_len);
            _recv_statu = Recv_Http_All;
            return true;
        }
        // 3.1 数据过多 3.2 数据过少
        // 数据太少了则将数据全部取出来，但是不修改Recv_statu
        _request._body.append(buf->ReadPosition(), buf->ReadAbleSize());
        buf->MoveReadOffSet(buf->ReadAbleSize());
        return true;
    }

public:
    HttpContext() : _resp_statu(200), _recv_statu(Recv_Http_Line) {}
    void Reset()
    {
        _recv_statu = Recv_Http_Line;
        _resp_statu = 200;
        _request.ReSet();
    }
    int GetResponseStatu() { return _resp_statu; }       // 获取响应装态码
    HttpRequest &GetRequest() { return _request; }       // 获取接收完毕并已经解析完的请求信息
    HttpRecvStatu GetRecvStatu() { return _recv_statu; } // 获取当前接收的状态
    // 开始接收，通过调用私有函数来进行接收
    void RecvHttpRequest(Buffer *buf)
    {
        switch (_recv_statu)
        {
        case Recv_Http_Line:
            RecvHttpLine(buf);
        case Recv_Http_Head:
            RecvHttpHead(buf);
        case Recv_Http_Body:
            RecvHttpBody(buf);
        }
        return;
    }
};
int n = 0;
class HttpSever
{
private:
    using Handler = std::function<void(const HttpRequest &, HttpResponse *)>;
    using Handlers = std::vector<std::pair<std::regex, Handler>>;
    Handlers _get_route;
    Handlers _post_route;
    Handlers _put_route;
    Handlers _delete_route;
    std::string _basedir;
    TCPSever _server;
private:
    void WriteResponse(const PtrConnection con, HttpRequest &request, HttpResponse &response)
    {
        // 1.完善头部字段
        //  1.长短连接
        //  2.是否有正文以及正文长度
        // 3.是否有正文以及正文类型
        if (request.Close() == true)
        {
            // true表示是长连接
            if (response.HasHeader("Connection") == false)
            {
                response.SetHeader("Connection", "keep-alive");
            }
        }
        else
        {
            if (response.HasHeader("Connection") == false)
            {
                response.SetHeader("Connection", "close");
            }
        }
        if (response._body.empty() == false && response.HasHeader("Content-Length") == false)
        {
            response.SetHeader("Content-Length", std::to_string(response._body.size()));
        }
        if (response._body.empty() == false && response.HasHeader("Content-Type") == false)
        {
            response.SetHeader("Content-Type", "application/otect-stream");
        }
        if (response._redirect_flag == true)
        {
            response.SetHeader("Location", response._redirect_url);
        }
        // 2.按照Http协议格式填充字符串
        std::stringstream rsp_str;
        rsp_str << request._version << " " << response._statu << " " << Util::GetDesc(response._statu) << "\r\n";
        for (auto head : response._hearders)
        {
            rsp_str << head.first << ": " << head.second << "\r\n";
        }
        rsp_str << "\r\n";
        rsp_str << response._body;
        // 3.发送数据
        con->Send(rsp_str.str().c_str(), rsp_str.str().size());
    }
    void ErrorHandler(HttpRequest &request, HttpResponse &response)
    {
        std::string body;
        body += "<html>";
        body += "<head>";
        body += "<meta http-equiv='Content-Type' content='text/html;charset=utf-8'>";
        body += "</head>";
        body += "<body>";
        body += "<h1>";
        body += std::to_string(response._statu);
        body += " : ";
        body += Util::GetDesc(response._statu);
        body += "</h1>";
        body += "</body>";
        body += "</html>";
        return;
    }
    bool IsFileHandler(HttpRequest &request)
    {
        // 1.首先判断是否有静态的根目录
        if (_basedir.empty() == true)
        {
            return false;
        }
        // 2.只有GET/HEAD方法才是静态的资源请求
        if (request._method != "GET" && request._method != "HEAD")
        {
            return false;
        }
        // 3.判断是否是有效的资源路径
        //  有可能请求的path只是一个单纯的 / ，这样就表示访问index.html
        //  也有可能就是单纯的访问 /image/a.jpg之类的，需要在前面加上默认根目录 --./www.root/image/a.jpg
        if (Util::IsValidPath(request._path) == false)
        {
            return false;
        }
        // ./wwwroot/index.html
        std::string req_path = _basedir + request._path;
        if (request._path.back() == '/')
        {
            req_path += "index.html";
        }
   
        // 4.判断该文件是否存在
        if (Util::IsRegular(req_path) == false)
        {
            return false;
        }
        request._path = req_path;
        return true;
    }
    // 静态请求
    void FileHandler(HttpRequest &request, HttpResponse *response)
    {
        // 通过request的资源路径找到对应的资源，然后填充到response的body中去
        // 并且还要填充一个Content-Type的头部字段
        bool ret = Util::ReadFile(request._path, &(response->_body));
        if (ret == false){
            return;
        }
        std::string mine = Util::ExcMine(request._path);
        response->SetHeader("Content-Type", mine);
        return;
    }
    // 功能性请求，根据不同的请求来使用不同的路由表
    void Dispatcher(HttpRequest &request, HttpResponse *response, Handlers &handlers)
    {
        // 通过正则库来对request的资源请求进行匹配
        for (auto &handler : handlers)
        {
            //唯一的问题是正则库没有东西
            const std::regex &re = handler.first;
            const Handler &functor = handler.second;

            bool ret = std::regex_match(request._path, request._matches, re);
            // 若是匹配失败，就直接continue来循环下一次
            if (ret == false)
            {
                continue;
            }
            // 若是成功了，就直接调用functor进行业务处理
            return functor(request, response);
        }
        // 最后若是循环结束依旧没有找到，就直接设置404
        response->_statu = 404;
    }
    // 开始路由，查看是静态请求还是功能性请求
    void Route(HttpRequest &request, HttpResponse *response)
    {
        // GET HEAD都是静态请求，先默认进行FileHandler
        if (IsFileHandler(request) == true)
        {
            return FileHandler(request, response);
        }
        if (request._method == "GET" || request._method == "HEAD")
        {
            return Dispatcher(request, response, _get_route);
        }
        else if (request._method == "PUT")
        {
            return Dispatcher(request, response, _put_route);
        }
        else if (request._method == "POST")
        {
            return Dispatcher(request, response, _post_route);
        }
        else if (request._method == "DELETE")
        {
            return Dispatcher(request, response, _delete_route);
        }
        // 若是都没有，就修该状态码
        response->_statu = 405; // Method Not Allowed;
    }
    // 设置上下文
    void OnConnected(PtrConnection con)
    {
        con->SetContext(HttpContext());
    }
    // 解析并处理请求
    void OnMessage(PtrConnection con, Buffer *buf)
    {
        while (buf->ReadAbleSize() > 0)
        {
            DEBUG_LOG("n = %d",n++);
            // 1.获取连接的上下文
            HttpContext *context = con->GetContext()->get<HttpContext>();
            // 2.处理上下文请求，并获得HttpRequest对象
            //   1.有可能请求出错了
            //   2.请求没出错
            context->RecvHttpRequest(buf);
            HttpRequest &request = context->GetRequest();
            HttpResponse response(context->GetResponseStatu());
            if (context->GetResponseStatu() >= 400)
            {
                // 大于等于400说明出现了错误，就关闭连接并返回
                ErrorHandler(request, response);
                WriteResponse(con, request, response);
                context->Reset();
                buf->MoveReadOffSet(buf->ReadAbleSize());
                con->Shutdown();
                return;
            }
            if (context->GetRecvStatu() != Recv_Http_All)
            {
                return;
            }
            // 3.进行路由，并进行业务处理
            Route(request, &response);
            // 4.进行业务处理后，获得HttpResponse对象，并发送过去
            WriteResponse(con, request, response);
            context->Reset();
            // 5.若是短连接，就需要直接shutdown这个连接
            if (request.Close() == false)
                con->Shutdown();
        }
    }

public:
    HttpSever(int port, int timeout = 30)
        : _server(port)
    {
        _server.EnableInactiveRelease(timeout);
        _server.SetConnectCallBack(std::bind(&HttpSever::OnConnected, this, std::placeholders::_1));
        _server.SetMessageCallBack(std::bind(&HttpSever::OnMessage, this, std::placeholders::_1, std::placeholders::_2));
    }
    void SetBaseDir(std::string filename)
    {
        bool ret = Util::IsDirectory(filename);
        assert(ret == true);
        _basedir = filename;
    }
    // 设置对应请求的路由表
    // 其中路由表所记录的函数名并不是单纯的一个函数名，而是一个正则表达式
    void Get(const std::string &pattern, const Handler handler)
    {
        _get_route.push_back(std::make_pair(std::regex(pattern), handler));
    }
    void Post(const std::string &pattern, const Handler handler)
    {
        _post_route.push_back(std::make_pair(std::regex(pattern), handler));
    }
    void Put(const std::string &pattern, const Handler handler)
    {
        _put_route.push_back(std::make_pair(std::regex(pattern), handler));
    }
    void Delete(const std::string &pattern, const Handler handler)
    {
        _delete_route.push_back(std::make_pair(std::regex(pattern), handler));
    }
    // 设置线程个数
    void SetThreadCount(int count)
    {
        _server.SetThreadCount(count);
    }
    // 启动服务器
    void Listen()
    {
        _server.Start();
    }
};
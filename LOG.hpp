#include<time.h>
using namespace std;

#define NORMAL 0
#define DEBUG 1
#define ERROR 2
//format是指输入的格式：如%s,%d等,写在后面会自动连接到""内去
//FILE则是指运行这个代码的文件名，LINE是指文件行数
//__VA_ARGS__则是指多个参数，前面加##则是告知编译器，没有多余参数就不用管
#define LOG(level,format, ...) do{\
    if(level < 1) break;\
    time_t t = time(NULL);\
    struct tm *localt = localtime(&t);\
    char tmp[32] = {0};\
    size_t n = strftime(tmp,31,"%H:%M:%S",localt);\
    fprintf(stdout,"[%s] [%s:%d]" format "\n",tmp,__FILE__,__LINE__,##__VA_ARGS__);\
    }while(0)
//format和前面的格式中间有一个空格

#define NORMAL_LOG(format,...) LOG(NORMAL,format,##__VA_ARGS__)
#define DEBUG_LOG(format,...) LOG(DEBUG,format,##__VA_ARGS__)
#define ERROR_LOG(format,...) LOG(ERROR,format,##__VA_ARGS__)

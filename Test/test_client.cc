#include"../Source/Sever.hpp"
#include"../Source/LOG.hpp"

int main()
{
    Socket Client;
    Client.CreateClient(8080,"127.0.0.1",1);
    std::string str("I am Client");
    std::cout<<str<<std::endl;
    Client.Send(str.c_str(),1023);
    char buf[1024] = {0};
    Client.Recv(buf,1023);
    std::cout<<buf<<std::endl;
    return 0;
}
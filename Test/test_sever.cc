#include"../Source/Sever.hpp"
#include"../Source/LOG.hpp"

int main()
{
    Socket Sever;
    Sever.CreateSever(8080,"0.0.0.0",1);
    while(true)
    {
        int cli_fd = Sever.Accept();
        if(cli_fd < 0) {
            DEBUG_LOG("Accept Fail");
            continue;
            }
        Socket cli(cli_fd);
        char buf[1024] = {0};
        int ret = cli.Recv(buf,1023);
        if(ret <= 0)
        {
            DEBUG_LOG("Recv Nothing");
            continue;
        }
        std::cout<<buf<<std::endl;
        std::string back("hello");
        cli.Send(back.c_str(),back.size());
    }
    return 0;
}
#include <string>
#include <iostream>
#include <vector>
#include "../Source/LOG.hpp"
#include "Http.hpp"
#include <fstream>
#include<unordered_map>
using namespace std;

static size_t Split(const string& src, const string sep, vector<string>* array)
{
    size_t offset = 0;
    while (offset < src.size())
    {
        size_t pos = src.find(sep, offset);
        if (pos == std::string::npos)
        {
            array->push_back(src.substr(offset));
            return array->size();
        }
        if (offset == pos)
        {
            offset = pos + sep.size();
            continue;
        }
        array->push_back(src.substr(offset, pos - offset));
        offset = pos + sep.size();
    }
    return array->size();
}
static bool ReadFile(const string& filename, string* buf)
{
    std::ifstream ifs(filename, std::ios::binary);
    if (ifs.is_open() == false)
    {
        ERROR_LOG("FILE OPEN FAILED!!");
        return false;
    }
    ifs.seekg(0, ifs.end);
    size_t fsize = ifs.tellg();
    ifs.seekg(0, ifs.beg);
    buf->resize(fsize);
    ifs.read(&(*buf)[0], fsize);
    if (ifs.good() == false)
    {
        ERROR_LOG("READ %s FILE FAILED!!", filename.c_str());
        ifs.close();
        return false;
    }
    ifs.close();
    return true;
}

static bool WriteFile(const string& filename, const string& buf)
{
    std::ofstream ofs(filename, std::ios::binary | std::ios::trunc);
    if (ofs.is_open() == false)
    {
        ERROR_LOG("FILE OPEN FAILED!!");
        return false;
    }
    ofs.write(buf.c_str(), buf.size());
    if (ofs.good() == false)
    {
        ERROR_LOG("WRITE %s FILE FAILED!!", filename.c_str());
        ofs.close();
        return false;
    }
    ofs.close();
    return true;
}
static string UrlEncode(std::string url, bool turn_space_to_plus)
{
    std::string ret;
    for (auto& s : url)
    {
        if (s == '.' || s == '-' || s == '_' || s == '~' || isalnum(s))
        {
            ret += s;
            continue;
        }
        if (s == ' ' && turn_space_to_plus == true)
        {
            ret += '+';
            continue;
        }
        char tmp[4] = { 0 };
        // 若是其他的就需要转换为%HH的格式
        snprintf(tmp, 4, "%%%02X", s);
        ret += tmp;
    }
    return ret;
}
// URL解码
static char HexToI(char c)
{
    if (c >= '0' && c <= '9')
    {
        return c - '0';
    }
    else if (c >= 'a' && c <= 'x')
    {
        return c - 'a' + 10;
    }
    else if (c >= 'A' && c <= 'X')
    {
        return c - 'A' + 10;
    }
    return -1;
}
static string UrlDecode(const string url, bool turn_plus_to_space)
{
    string ret;
    for (int i = 0; i < url.size(); i++)
    {
        if (url[i] == '+' && turn_plus_to_space == true)
        {
            ret += url[i];
            continue;
        }
        if (url[i] == '%')
        {
            char v1 = HexToI(url[i + 1]);
            char v2 = HexToI(url[i + 2]);
            char v = (v1 << 4) + v2;
            ret += v;
            i += 2;
            continue;
        }
        ret += url[i];
    }
    return ret;
}
static string GetDesc(int statu)
{
    std::unordered_map<int, std::string> _statu_msg = {
        {100, "Continue"},
        {101, "Switching Protocol"},
        {102, "Processing"},
        {103, "Early Hints"},
        {200, "OK"},
        {201, "Created"},
        {202, "Accepted"},
        {203, "Non-Authoritative Information"},
        {204, "No Content"},
        {205, "Reset Content"},
        {206, "Partial Content"},
        {207, "Multi-Status"},
        {208, "Already Reported"},
        {226, "IM Used"},
        {300, "Multiple Choice"},
        {301, "Moved Permanently"},
        {302, "Found"},
        {303, "See Other"},
        {304, "Not Modified"},
        {305, "Use Proxy"},
        {306, "unused"},
        {307, "Temporary Redirect"},
        {308, "Permanent Redirect"},
        {400, "Bad Request"},
        {401, "Unauthorized"},
        {402, "Payment Required"},
        {403, "Forbidden"},
        {404, "Not Found"},
        {405, "Method Not Allowed"},
        {406, "Not Acceptable"},
        {407, "Proxy Authentication Required"},
        {408, "Request Timeout"},
        {409, "Conflict"},
        {410, "Gone"},
        {411, "Length Required"},
        {412, "Precondition Failed"},
        {413, "Payload Too Large"},
        {414, "URI Too Long"},
        {415, "Unsupported Media Type"},
        {416, "Range Not Satisfiable"},
        {417, "Expectation Failed"},
        {418, "I'm a teapot"},
        {421, "Misdirected Request"},
        {422, "Unprocessable Entity"},
        {423, "Locked"},
        {424, "Failed Dependency"},
        {425, "Too Early"},
        {426, "Upgrade Required"},
        {428, "Precondition Required"},
        {429, "Too Many Requests"},
        {431, "Request Header Fields Too Large"},
        {451, "Unavailable For Legal Reasons"},
        {501, "Not Implemented"},
        {502, "Bad Gateway"},
        {503, "Service Unavailable"},
        {504, "Gateway Timeout"},
        {505, "HTTP Version Not Supported"},
        {506, "Variant Also Negotiates"},
        {507, "Insufficient Storage"},
        {508, "Loop Detected"},
        {510, "Not Extended"},
        {511, "Network Authentication Required"} };
    auto ret = _statu_msg.find(statu);
    if (ret == _statu_msg.end())
    {
        return "Unkown";
    }
    return ret->second;
}
static string ExcMine(string str)
{
    std::unordered_map<std::string, std::string> _mime_msg = {
        {".aac", "audio/aac"},
        {".abw", "application/x-abiword"},
        {".arc", "application/x-freearc"},
        {".avi", "video/x-msvideo"},
        {".azw", "application/vnd.amazon.ebook"},
        {".bin", "application/octet-stream"},
        {".bmp", "image/bmp"},
        {".bz", "application/x-bzip"},
        {".bz2", "application/x-bzip2"},
        {".csh", "application/x-csh"},
        {".css", "text/css"},
        {".csv", "text/csv"},
        {".doc", "application/msword"},
        {".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
        {".eot", "application/vnd.ms-fontobject"},
        {".epub", "application/epub+zip"},
        {".gif", "image/gif"},
        {".htm", "text/html"},
        {".html", "text/html"},
        {".ico", "image/vnd.microsoft.icon"},
        {".ics", "text/calendar"},
        {".jar", "application/java-archive"},
        {".jpeg", "image/jpeg"},
        {".jpg", "image/jpeg"},
        {".js", "text/javascript"},
        {".json", "application/json"},
        {".jsonld", "application/ld+json"},
        {".mid", "audio/midi"},
        {".midi", "audio/x-midi"},
        {".mjs", "text/javascript"},
        {".mp3", "audio/mpeg"},
        {".mpeg", "video/mpeg"},
        {".mpkg", "application/vnd.apple.installer+xml"},
        {".odp", "application/vnd.oasis.opendocument.presentation"},
        {".ods", "application/vnd.oasis.opendocument.spreadsheet"},
        {".odt", "application/vnd.oasis.opendocument.text"},
        {".oga", "audio/ogg"},
        {".ogv", "video/ogg"},
        {".ogx", "application/ogg"},
        {".otf", "font/otf"},
        {".png", "image/png"},
        {".pdf", "application/pdf"},
        {".ppt", "application/vnd.ms-powerpoint"},
        {".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"},
        {".rar", "application/x-rar-compressed"},
        {".rtf", "application/rtf"},
        {".sh", "application/x-sh"},
        {".svg", "image/svg+xml"},
        {".swf", "application/x-shockwave-flash"},
        {".tar", "application/x-tar"},
        {".tif", "image/tiff"},
        {".tiff", "image/tiff"},
        {".ttf", "font/ttf"},
        {".txt", "text/plain"},
        {".vsd", "application/vnd.visio"},
        {".wav", "audio/wav"},
        {".weba", "audio/webm"},
        {".webm", "video/webm"},
        {".webp", "image/webp"},
        {".woff", "font/woff"},
        {".woff2", "font/woff2"},
        {".xhtml", "application/xhtml+xml"},
        {".xls", "application/vnd.ms-excel"},
        {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
        {".xml", "application/xml"},
        {".xul", "application/vnd.mozilla.xul+xml"},
        {".zip", "application/zip"},
        {".3gp", "video/3gpp"},
        {".3g2", "video/3gpp2"},
        {".7z", "application/x-7z-compressed"} };
    size_t pos = str.find_last_of('.');
    if (pos == string::npos)
    {
        return "application/otect-stream";
    }
    auto it = _mime_msg.find(str.substr(pos));
    if (it == _mime_msg.end())
    {
        return "application/otect-stream";
    }
    return it->second;
}
static bool IsDirectory(const string& filename)
{
    struct stat st;
    int ret = stat(filename.c_str(), &st);
    if (ret < 0)
    {
        return false;
    }
    return S_ISDIR(st.st_mode);
}
// 判断一个文件是否是普通文件
static bool IsRegular(const string& filename)
{
    struct stat st;
    int ret = stat(filename.c_str(), &st);
    if (ret < 0)
    {
        return false;
    }
    return S_ISREG(st.st_mode);
}
static bool IsValidPath(const string& filename)
{
    std::vector<string> array;
    Split(filename, "/", &array);
    int level = 0;
    for (auto& t : array)
    {
        if (t == "..")
        {
            level--;
            if (level < 0)
                return false;
            continue;
        }
        level++;
    }
    return true;
}
int main()
{
    cout << IsValidPath("/../index.html") << endl;
    // cout<<IsDirectory("testdir")<<endl;
    // cout<<IsDirectory("test.cc")<<endl;
    // cout<<IsRegular("testdir")<<endl;
    // cout<<IsRegular("test.cc")<<endl;
    // string str = "a.txt";
    // string ret = ExcMine(str);
    // cout<<ret<<endl;
    // int statu = 404;
    // string ret = GetDesc(statu);
    // cout<<ret<<endl;
    // string str = "C++";
    // string ret = UrlEncode(str, false);
    // string tmp = UrlDecode(ret, false);
    // cout << ret << endl;
    // cout << tmp << endl;
    // string buf("Play Games");
    // WriteFile("test.txt",buf);
    // string buf;
    // ReadFile("test.txt", &buf);
    // cout << buf << endl;
    // string str = "abc";
    // vector<string> array;
    // Split(str, ",", &array);
    // for (auto &s : array)
    // {
    //     cout << "[" << s << "]" << endl;
    // }
    return 0;
}